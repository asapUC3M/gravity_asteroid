function Coe_Comp(t,tt,OES,OET,ct)

% [p,a,ecc,incl,omega,argp,nu,m,arglat,truelon,lonper ]

yy{1} = 'Semi-Latus Rectum [m]';
yy{2} = 'Semi-Major Axis [m]';
yy{3} = 'Eccentricity []';
yy{4} = 'Inclination [rads]';
yy{5} = 'Longitude of Ascending Node [rads]';
yy{6} = 'Argument of Periapsis [rads]';
yy{7} = 'True Anomaly [rads]';
yy{8} = 'Mean Anomaly [rads]';

figure(1)

for k = 1 : 4
    
    subplot(2,2,k)
    
    semilogx(t,OES(:,k),tt(1:ct),OET(1:ct,k),'.-k');
    legend('Simulation','Celestrack TLE Data');
    xlabel('Time [secs]');
    ylabel(yy{k});
    
    
    
end

figure(2)

for k = 1 : 4
    
    subplot(2,2,k)
    
    plot(t,OES(:,k+4),tt(1:ct),OET(1:ct,k+4),'.-k');
    legend('Simulation','Celestrack TLE Data');
    xlabel('Time [secs]');
    ylabel(yy{k+4});
    
    
    
end

figure(3)

for k = 1 : 4
    
    subplot(2,2,k)
    
    e_rel = abs(interp1(t,OES(:,k),tt(1:ct))-OET(1:ct,k))./interp1(t,OES(:,k),tt(1:ct));
    
    loglog(tt(1:ct),e_rel,'.-k');
    xlabel('Time [secs]');
    ylabel(['Relative Error in ' yy{k}]);
    
    
    
end



