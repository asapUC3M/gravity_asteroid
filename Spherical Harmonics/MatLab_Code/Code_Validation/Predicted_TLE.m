clc
clear all
% close all

GM= 3.9860044150e+14;                   % m^3/s^2 
R = 6.3781363000e+06;                   % m

load('itggoce02_cell_modified.mat');
[~, NN] = size(itggoce02_cell);         % Model Size Evaluation


%% Declaring Initial Conditions For Integrator

DATA = Read_TLE('landsat4.txt');
[T0,R0,V0] = TLE2RV(DATA{1},GM);        %T0 initial time, R0 Initial position vector, V0 initial Velocity Vector

%% Integrator Set-Up

t0 = 0; tf = 1e5; 
tspan = [t0 tf];                        % Interval Of Time Of Integration [secs]

tol     = 1e-12;
opt = odeset('RelTol',tol,'AbsTol',[tol tol tol tol tol tol]);

%%% Integrator Call (NOTE: INTEGRATION IS DONE IN CARTESIAN COORDINATES)

tic

[t,X] = ode113(@ Field_Integrator,tspan,[R0;V0],opt,GM,R,itggoce02_cell,36,tf);

T = toc;                 


for i = 1 : length(t)
   
    [p,a,ecc,incl,omega,argp,nu,m,arglat,truelon,lonper ] = rv2coe(X(i,1:3),X(i,4:6),GM);
    OES(i,:) = [p,a,ecc,incl,omega,argp,nu,m,arglat,truelon,lonper ];
    
end

% Computational Time Spend

fprintf('\nIt took %1.3f [secs] to Compute %1.3f Real Time Simulation',T,tf-t0)
fprintf('\nIt takes %1.3f [secs] to compute one real second (Simulation Average)\n',T/(tf-t0));

tt = nan(length(DATA)-1,1);
XX = nan(length(DATA)-1,6);
for i = 1 : (length(DATA)-1)

    [t2,rr,vv] = TLE2RV(DATA{i},GM);
    [p,a,ecc,incl,omega,argp,nu,m,arglat,truelon,lonper ] = rv2coe(rr,vv,GM);
    OET(i,:) = [p,a,ecc,incl,omega,argp,nu,m,arglat,truelon,lonper ];
    tt(i) = t2 - T0;
    if tt(i) < tf
        ct = i;
    end
    XX(i,:) = [rr' vv'];
    
end

tt(1) = t(2);
Coe_Comp(t,tt,OES,OET,ct);
% Simulation(t,X,R);

