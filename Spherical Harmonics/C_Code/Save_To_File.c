#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>


void Save_To_File(char *FileName, double *Results[6],double* T,int N_steps){

	int  i;
	FILE *f;
	
	f = fopen(FileName, "w+");

	if(f==NULL){
		printf("\nError Saving Results\n");
	}

	else{


	for(i=0;i<=N_steps;i++){

			fprintf(f,"%f	%f	%f	%f	%f	%f	%f\n",T[i],Results[0][i],Results[1][i],Results[2][i],Results[3][i],Results[4][i],Results[5][i]);

	}

	fclose(f);
	}
}