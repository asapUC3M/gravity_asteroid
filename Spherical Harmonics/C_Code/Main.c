#include "functions.h"


void main(){


	// Declaring Initial Conditions For Integrator

	// Initial Position

	double r,phi,lambda,x,y,z;
	double vx;
	double vy;
	double vz;
	double t0,tf;
	double *Initial_State;
	int j = 0;
	int N_steps;
	double *StateOutput[6];
	double *T;				// It is a 7 row Matrix for: x,y,z,vx,vy,vz,t
	char *Name = {"Sim.csv"};

	r = 1.1*R;								// Distance From Earth CM
	phi = pi/4;								// Planet Latitude           (Geocentric)
	lambda = pi/5;

	x = 0;
	y = 0;
	z = 0;

	// Planet Longitude          (Geocentric)

	sph2cart(r,phi,lambda, &x, &y, &z);       // Cartesian Transformation

	// Initial Velocity

	vx    = 7500;                           // [m/s]
	vy    = 7.2902 *cos(30.0);              // [m/s]
	vz    = 7.2902 *sin(30.0);              // [m/s]


	/* Integrator Initial State Declaration */

	t0 = 0.0;
	tf = pow(10,-8);

	Initial_State = (double*) calloc(5,sizeof(double));

	Initial_State[0] = x;
	Initial_State[1] = y;
	Initial_State[2] = z;
	Initial_State[3] = vx;
	Initial_State[4] = vy;
	Initial_State[5] = vz;

	N_steps = 3;

	StateOutput[0]   = (double*) calloc(N_steps,sizeof(double));	// x  position
	StateOutput[1]   = (double*) calloc(N_steps,sizeof(double));	// y  position
	StateOutput[2]   = (double*) calloc(N_steps,sizeof(double));	// z  position
	StateOutput[3]   = (double*) calloc(N_steps,sizeof(double));	// vx position
	StateOutput[4]   = (double*) calloc(N_steps,sizeof(double));	// vy position
	StateOutput[5]   = (double*) calloc(N_steps,sizeof(double));	// vz position

	T  = (double*) calloc(N_steps,sizeof(double));

	RK4_Integrator(Field_Integrator,t0,tf,Initial_State,N_steps,StateOutput,T);
	Save_To_File(Name, StateOutput, T, N_steps);

	printf("\n");
	system("PAUSE");
}
