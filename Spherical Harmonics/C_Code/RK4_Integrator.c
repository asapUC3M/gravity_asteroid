#include "functions.h"

void RK4_Integrator(double* ODE(double t, double X[], double *Cnm, double *Snm), double t0, double tf, double *Initial_State, int N_steps, double *State_Output[5], double *T){

	int i = 0;
	int j = 0;
	double XX[6];				// Auxiliary State Vector
	double dt;
	double *Cnm,*Snm;

	double *k1,*k2,*k3,*k4;

	/* Integrator Functions */

	k1 = (double*) calloc(6,sizeof(double));
	k2 = (double*) calloc(6,sizeof(double));
	k3 = (double*) calloc(6,sizeof(double));
	k4 = (double*) calloc(6,sizeof(double));


	/* Function Coefficients Acquisition*/

	Cnm = (double*) calloc(29160,sizeof(double));
	Snm = (double*) calloc(29160,sizeof(double));

	Cnm = Coefficients_Cnm();
	Snm = Coefficients_Snm();

	/* Integration Initialize */


	dt = (double) (tf-t0)/N_steps;


	/* Allocate Initial State */

	State_Output[0][0] = Initial_State[0];
	State_Output[1][0] = Initial_State[1];
	State_Output[2][0] = Initial_State[2];
	State_Output[3][0] = Initial_State[3];
	State_Output[4][0] = Initial_State[4];
	State_Output[5][0] = Initial_State[5];


	T[0] = t0;



	/* Initialize Integrator */

	for(i=0;i<N_steps;i++){

		for(j=0;j<=5;j++){
			XX[j] = State_Output[j][i];
		}

		k1 = ODE(T[i], XX, Cnm, Snm);

		for(j=0;j<=5;j++){
			XX[j] = State_Output[j][i] + 0.5*k1[j];
		}

		k2 = ODE(T[i], XX, Cnm, Snm);  

		for(j=0;j<=5;j++){
			XX[j] = State_Output[j][i] + 0.5*k2[j];
		}

		k3 = ODE(T[i], XX, Cnm, Snm);  

		for(j=0;j<=5;j++){
			XX[j] = State_Output[j][i] + k3[j];
		}

		k4 = ODE(T[i], XX, Cnm, Snm);

		for(j=0;j<=5;j++){
			State_Output[j][i+1] = State_Output[j][i] + dt/6.0*(k1[j] + 2*k2[j] + 2*k3[j] + k4[j]) ;
		}

		T[i+1] = T[i] + dt ;

		printf("\n Progress %f %  time t = %f \n x = %f; y = %f; z= %f ",(double) i/N_steps*100,T[i],State_Output[0][i],State_Output[1][i],State_Output[2][i]);

	}

	printf("\n Progress %f %  time t = %f \n x = %f; y = %f; z= %f ",(double) i/N_steps*100,T[i],State_Output[0][i],State_Output[1][i],State_Output[2][i]);



}