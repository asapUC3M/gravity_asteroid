#include "functions.h"

void Normalized_Legendre(double P[], double Pn_1[], double Pn_2[], double sinphi, double cosphi, int n){

	//   Recursive Computation Of The Associated Legendre Polynomials of Order n

	//   Author: Diego Garc�a Pardo (UNIVERSITY CARLOS III OF MADRID)
	//   
	//   
	//   Requires as Input lower order Legendre Polynomials P @ n-1 and P @ n-2
	//   
	//   Also requires the introduction of the values of the sine and cosine of
	//   the longitude angle (measured from the x-y plane (horizontal with
	//   z-axis over the north pole)
	//   
	//   Therefore this function is ONLY VALID for greater or equal than 2 polynomial

	//  ONLY VALID FOR n > 2

	int i = 0;
	int *m;
	double chi_1,chi_2,chi_3,chi_4;

	m = (int*) calloc((n+1),sizeof(int));

	// Create Vector m (ALF)
	for(i=0; i<=n ;i++){
		m[i] = i;
	}


	for(i=0;i<=(n-2);i++){

		// Normalizing Parameters
		chi_1 =  sqrt( (double)  (2*n+1)*(n-m[i]) /( (2*n-1)*(n+m[i]) )  );
		chi_2 =  sqrt( (double)  (2*n+1)*(n-m[i])*(n-m[i]-1) / ( (2*n-3)*(n+m[i])*(n+m[i]-1) )  );

		// Legendre Polynomial Value
		P[i] =  (double) 1/(n-m[i]) * ( (2*n-1)*sinphi*Pn_1[i]*chi_1 - ( n + m[i] -1)*Pn_2[i]*chi_2 );

	}

	// Additional Normalizing Parameters
	chi_3 = sqrt( (double) 2*n+1 );
	chi_4 = sqrt( (double) (2*n+1) / (2*n)  );

	// Resting Legendre Polynomial Values
	P[n-1]	= (double) sinphi *  Pn_1[n-1] * chi_3;
	P[n]	= (double) cosphi *  Pn_1[n-1] * chi_4;

	CopyBackRecursion(P,Pn_1,Pn_2,n);
	

}
