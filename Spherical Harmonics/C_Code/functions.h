#define pi				3.14159265358979323846264338327950288419716939937510582097494459230781640628620899862803482534211706798214808651328230665 // Mathematical Constant
#define GM				3.9860044150e+14				// Standard Gravity Parameter	
#define R				6.3781363000e+06				// Earth Mean Radious (source: ITG-GOCE DATA FILE)
#define itg_goce_size	240								// ITG-GOCE MODEL SIZE


#include<stdio.h>
#include<stdlib.h>
#include<math.h>

void main();

void		ManualLegendre(double P[], double Pn_1[],double Pn_2[], double sinphi, double cosphi);

void		initializeALF(double* P, double* Pn_1,double* Pn_2,int N);

void		Normalized_Legendre(double P[], double Pn_1[], double Pn_2[], double sinphi, double cosphi, int n);

void		cart2sph(double x,double y, double z, double *r, double *phi, double *lambda);

void		CopyBackRecursion(double P[], double Pn_1[], double Pn_2[], int n);

void		sph2cart(double r,double phi, double lambda, double* x, double* y, double* z);

double*		FictAcc(double v[], double r[]);

double*		Field_Integrator(double t, double X[], double *Cnm, double *Snm);

double*		Acc_Field(double r, double phi, double lambda, double *Cnm, double *Snm);

double*		Coefficients_Cnm();

double*		Coefficients_Snm();

void		RK4_Integrator(double* ODE(double t, double X[], double *Cnm, double *Snm),double t0, double tf, double *Initial_State, int N_steps, double *State_Output[5], double *T);

void		Save_To_File(char *FileName, double *Results[6],double* T,int N_steps);