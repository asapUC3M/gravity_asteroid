#include "functions.h"


#define pi				3.14159265358979323846264338327950288419716939937510582097494459230781640628620899862803482534211706798214808651328230665 // Mathematical Constant
#define GM				3.9860044150e+14				// Standard Gravity Parameter	
#define R				6.3781363000e+06				// Earth Mean Radious (source: ITG-GOCE DATA FILE)
#define itg_goce_size	240								// ITG-GOCE MODEL SIZE



double* Acc_Field(double r, double phi, double lambda, double *Cnm, double *Snm){

	// Computation Of Inertial Acceleration Field Associtaed with Planetary Characteristics:

	//  Author: Diego Garc�a Pardo (UNIVERSITY CARLOS III OF MADRID)

	//      --Std. Gravitational Constant       GM  [m^3 s^-2]
	//      --Planet Radius                     R   [m]
	//      --Space Location
	//              - Distance from Planet CM   r   [m]
	//              - Longitude                 phi [rads]
	//              - Latitude                  lambda [rads]

	//      --Geopotential Coefficients (Fully Normalized)
	//              - Cell Size of NNx2, 1st column Cnm 2nd Snm
	//              
	//  It is used the scheme shown in Montebruck (Satellite Orbits)

	//  NOTE: This Algorithm requires NN >= 2

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// Function Local Variables;
	double* rdotdot;
	double *V, *W;
	double NF1,NF2,NF3;
	double *CC,*SS;
	double d2x=0;
	double d2y=0;
	double d2z=0;
	int n,N,m,M;
	int i =0;
	int I = 0;
	double *P, *Pn_1,*Pn_2;

	double sinphi, cosphi;

	rdotdot = (double*) calloc(3,sizeof(double));
	V = (double*) calloc(itg_goce_size,sizeof(double));
	W = (double*) calloc(itg_goce_size,sizeof(double));

	CC = (double*) calloc(itg_goce_size,sizeof(double));
	SS = (double*) calloc(itg_goce_size,sizeof(double));

	sinphi = sin(phi);
	cosphi = cos(phi);


	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// Allocating Legendre Vectors



	P		= (double*) calloc(itg_goce_size,sizeof(double));
	Pn_1	= (double*) calloc(itg_goce_size,sizeof(double));
	Pn_2	= (double*) calloc(itg_goce_size,sizeof(double));

	ManualLegendre(P,Pn_1,Pn_2,sinphi,cosphi);

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// Zero Order Model

	n = 0;									// Model Order Counter
	m = 0;									// Associated Index (Degree)
	N = n+1;								// Counters for functions V & W (which are 1 order higher)

	for(i=0;i<=n;i++){

		CC[i] = Cnm[i+I];
		SS[i] = Snm[i+I];

	}
	I = I + n + 1;							// Vector Counter in Snm And Cnm

	// Zonal Coefficients Associated Acceleration @(m = 0)

	for(M=0;M<=N;M++){

		V[M] = pow(R/r,(N+1)) * Pn_1[M] * cos(M*lambda);
		W[M] = pow(R/r,(N+1)) * Pn_1[M] * sin(M*lambda);

	}

	NF1  = sqrt((double) 0.5 * (2*n+1) * (n+2) * (n+1) / (2*n+3) );
	NF2  = sqrt((double) (2*n+1) * (n+m+1) / (2*n+3) / (n-m+1) );

	d2x		= -CC[m] * V[m+1] * NF1;
	d2y		= -CC[m] * W[m+1] * NF1;
	d2z		= (n+1)*(-CC[m]*V[m] - SS[m]*W[m]) * NF2;

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// Counter increase to order 1

	n = n+1;            // n = 1;
	N = n+1;

	for(i=0;i<=n;i++){

		CC[i] = Cnm[i+I];
		SS[i] = Snm[i+I];

	}
	I = I + n + 1;

	Normalized_Legendre(P,Pn_1,Pn_2,sinphi,cosphi,N);

	for(M=0;M<=N;M++){

		V[M] = pow(R/r,(N+1)) * Pn_1[M] * cos(M*lambda);
		W[M] = pow(R/r,(N+1)) * Pn_1[M] * sin(M*lambda);

	}

	// Zonal Coefficients Associated Acceleration @(m = 0)
	m = 0;
	NF1  = sqrt((double) 0.5*(2*n+1)*(n+2)*(n+1)/(2*n+3));
	NF2  = sqrt((double) (2*n+1)*(n+m+1)/(2*n+3)/(n-m+1));

	d2x		= -CC[m] * V[m+1] * NF1						+d2x;
	d2y		= -CC[m] * W[m+1] * NF1						+d2y;
	d2z		= (n-m+1)*(-CC[m]*V[m] - SS[m]*W[m]) * NF2	+d2z;


	// Acceleration @(m = 1), Scheme Exception Due To Normalization

	m = 1;
	NF1 = sqrt((double) (2*n+1)/(2*n+3)*(n+m+2)*(n+m+1));
	NF2 = sqrt((double) 2*(2*n+1)/(2*n+3)/(n-m+2)/(n-m+1));
	NF3 = sqrt((double) (2*n+1)*(n+m+1)/(2*n+3)/(n-m+1));



	d2x = (double)  0.5*( (-CC[m]*V[m+1] - SS[m]*W[m+1]) * NF1 + (n-m+2)*(n-m+1)*(+CC[m]*V[m-1] + SS[m]*W[m-1]) * NF2 )		+ d2x;
	d2y = (double)	0.5*( (-CC[m]*W[m+1] + SS[m]*V[m+1]) * NF1 + (n-m+2)*(n-m+1)*(-CC[m]*V[m-1] + SS[m]*V[m-1]) * NF2 )		+ d2y;
	d2z = (double)  (n-m+1)*(-CC[m]*V[m] - SS[m]*W[m]) * NF3 																+ d2z;


	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// End Of Scheme Exceptions, LOOP

	for(n=2;n<(itg_goce_size-1);n++){

		N = n+1;

		for(i=0;i<=n;i++){

			CC[i] = Cnm[i+I];
			SS[i] = Snm[i+I];

		}
		I = I + n + 1;

		Normalized_Legendre(P,Pn_1,Pn_2,sinphi,cosphi,N);

		for(M=0;M<=N;M++){

			V[M] = pow(R/r,(N+1)) * Pn_1[M] * cos(M*lambda);
			W[M] = pow(R/r,(N+1)) * Pn_1[M] * sin(M*lambda);

		}

		// Zonal Coefficients Associated Acceleration @(m = 0)
		m = 0;
		NF1  = sqrt( (double) 0.5*(2*n+1)*(n+2)*(n+1)/(2*n+3));
		NF2  = sqrt( (double) (2*n+1)*(n+m+1)/(2*n+3)/(n-m+1));

		d2x		= -CC[m] * V[m+1] * NF1						+d2x;
		d2y		= -CC[m] * W[m+1] * NF1						+d2y;
		d2z		= (n-m+1)*(-CC[m]*V[m] - SS[m]*W[m]) * NF2	+d2z;

		// Acceleration @(m = 1), Scheme Exception Due To Normalization

		m = 1;
		NF1 = sqrt((double) (2*n+1)/(2*n+3)*(n+m+2)*(n+m+1));
		NF2 = sqrt((double) 2*(2*n+1)/(2*n+3)/(n-m+2)/(n-m+1));
		NF3 = sqrt((double) (2*n+1)*(n+m+1)/(2*n+3)/(n-m+1));

		d2x = (double)   0.5*( (-CC[m]*V[m+1] - SS[m]*W[m+1]) * NF1 + (n-m+2)*(n-m+1)*(+CC[m]*V[m-1] + SS[m]*W[m-1]) * NF2 )	+ d2x ;
		d2y = (double)	 0.5*( (-CC[m]*W[m+1] + SS[m]*V[m+1]) * NF1 + (n-m+2)*(n-m+1)*(-CC[m]*V[m-1] + SS[m]*V[m-1]) * NF2 )		+ d2y;
		d2z = (double)  (n-m+1)*(-CC[m]*V[m] - SS[m]*W[m]) * NF3 																+ d2z;


		// Generality of Terms

		for(m=2;m<=n;m++){

			NF1 = sqrt((double) (2*n+1)/(2*n+3)*(n+m+2)*(n+m+1));
			NF2 = sqrt((double) (2*n+1)/(2*n+3)/(n-m+2)/(n-m+1));
			NF3 = sqrt((double) (2*n+1)*(n+m+1)/(2*n+3)/(n-m+1));

			d2x = d2x   +  0.5*( (-CC[m]*V[m+1] - SS[m]*W[m+1] ) * NF1 + (n-m+2)*(n-m+1)*(+CC[m]*V[m-1] + SS[m]*W[m-1]) * NF2 ) ;
			d2y = d2y   +  0.5*( (-CC[m]*W[m+1] + SS[m]*V[m+1] ) * NF1 + (n-m+2)*(n-m+1)*(-CC[m]*V[m-1] + SS[m]*V[m-1]) * NF2 ) ;
			d2z = d2z   +  (n-m+1)*(-CC[m]*V[m] - SS[m]*W[m]) * NF3;


		}

	}

	rdotdot[0] = d2x * GM/pow(R,2);
	rdotdot[1] = d2y * GM/pow(R,2);
	rdotdot[2] = d2z * GM/pow(R,2);

	return (double*) rdotdot;
}