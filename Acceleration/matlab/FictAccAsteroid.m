function  d2r_fict = FictAcc(v,r,w)

%{

    Non-Inertial Acceleration In Asteroid Fixed Frame due to Asteroid Spin

    Author: Diego Garc�a Pardo (UNIVERSITY CARLOS III OF MADRID)

    As the acceleration must be presented in cartesian coordinates, 
    no especial considerations about gradients must be taken into account

    It is assumed that the Earth Rotation Speed is Constant

    v & r must be column vectors
    
%}

coriolis = 2.*[-v(2)*w ; v(1)*w ; 0];
centrifugal = [-r(1)*w^2; -r(2)*w^2; 0];

d2r_fict = coriolis + centrifugal;


end