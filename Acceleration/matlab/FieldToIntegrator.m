function dX = FieldToIntegrator(t,X,GS,EE,FF,E,Asteroid,w_Asteroid,tf)
%{

    Universidad Carlos III de Madrid
    Author: Diego Garc�a Pardo

    This Function outputs formatted the value of the position derivatives
    so that they can be numerically integrated.

    Function inputs:

    -- t: Time of integration [secs]

    -- X: State of the body, X(1:3) are cartesian coordinates of the
    position. X(4:6) are the cartesian components of velocity

    -- GS: Product of G (Universal Gravitational Constant and S, Asteroid
    expected mean density. (SI Units).

    -- EE: is a rectangular matrix containing the dyad per edge in the
    polyhedron approximation to the asteroid shape.

    -- FF: is a rectangular matrix containing the outer product of the
    normal of each face.

    -- E:  is a structured variable containing information about edges
    location as well as the faces sharing such edge. A more detailed
    description is given in "PolyhedronParseV5.m". Mainly It is useful as
    it has the vertex coordinates per edge

    -- Asteroid is a structured variable as E containing information about
    asteroid. Mainly it contains the coordinates of vertex per face.

    -- w_Asteroid: Asteroid angular speed. Assumming rotation only about
    z-axis

%}
    
    Acc = Acceleration(X(1:3),GS,EE,FF,E,Asteroid);
%     d2r_fict = FictAccAsteroid(X(4:6),X(1:3),w_Asteroid);
    d2r_fict = 0;
%     v_fict = [-X(2)*w_Asteroid; X(1)*w_Asteroid; 0];
%     Velocity_wrt_Asteroid = X(4:6) - v_fict;
%     dX = [Velocity_wrt_Asteroid;Acc];

    dX = [X(4:6);(Acc-d2r_fict)]; 
    
    clc
    fprintf('\nProgress %1.4E %',t/tf*100);
    
end

