function wf = SolidAngle_wf(PosVector,Asteroid)

wf = nan(Asteroid.Faces.Counter,1);

rr = nan(3,3);      % Vector From Field Point to Vertex 
rrN = nan(3,1);     % Modulus of previous vector

for i = 1 : Asteroid.Faces.Counter
    
    for j = 1 : 3 %Only Triangles Are Computed
        rr(j,:) =  Asteroid.Vertex.Loc(Asteroid.Faces.Order(i,j),:) - PosVector'; 
        rrN(j) = norm(rr(j,:),2);
    end
    
    Num = dot(rr(1,:),cross(rr(2,:),rr(3,:)));
    Den = rrN(1)*rrN(2)*rrN(3) + rrN(1)*dot(rr(2,:),rr(3,:)) + rrN(2)*dot(rr(3,:),rr(1,:)) + rrN(3)*dot(rr(1,:),rr(2,:));
    
    wf(i) = 2*atan2(Num,Den);
    
end


end

