clc
clear all
close all
format long

S = 5.52e3;              % Kg/m^3;        Mean Density
G = 6.67384e-11;        % m^3/Kg/s^2     Gravitational Constant
GS = G*S;               % Product Density x Gravity Constant s^(-2)
R = 6.3781363000e+06;

Asteroid = ImportAsteroid('Castalia.txt');      % Function to process the geometry into a structured variable
AsteroidPlotter(Asteroid)

R = 0;
for i = 1 : length(Asteroid.Faces.Order(:,1))
    
    for j = 1 : 3
       
        aux = norm(Asteroid.Vertex.Loc(Asteroid.Faces.Order(i,j),:),2);
    
        if R < aux
            R = aux;
            LOC = Asteroid.Vertex.Loc(Asteroid.Faces.Order(i,j),:)*1.01;
        end
    end
end

hold on

thdom = linspace(0,pi,100);
phdom = linspace(-pi/2,pi/2,100);
[thdom,phidom]=meshgrid(thdom,phdom);
x = R.*cos(thdom).*cos(phidom);
y = R.*sin(thdom).*cos(phidom);
z = R.*sin(phidom);
hSurface  = surf(x,y,z);
set(hSurface,'FaceColor',[1 0 0]);
plot3(LOC(1),LOC(2),LOC(3),'*','Color',[0.45,0.75,0.25],'MarkerSize',25);
legend('Castalia Asteroid','Reference Sphere','Point Taken for Reference Radius');
xlabel('meters');
ylabel('meters');
zlabel('meters');