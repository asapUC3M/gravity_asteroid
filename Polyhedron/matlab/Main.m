clc
clear all
close all
format long

S = 2.1e3;              % Kg/m^3;        Mean Density
G = 6.67384e-11;        % m^3/Kg/s^2     Gravitational Constant
GS = G*S;               % Product Density x Gravity Constant s^(-2)            
R = 881.1147;

Asteroid = ImportAsteroid('Castalia.txt');              % Function to process the geometry into a structured variable
[EE,FF,E] = PolyhedronParsev5(Asteroid);                % This function Generates the Dyad's E and F from the structured developed in "ImportAsteroid"

PosVector = [R;R;R]/sqrt(3);                            % Position in Space where to evaluate U and Acc

tic
Acc = Acceleration(PosVector,GS,EE,FF,E,Asteroid);
toc
A = norm(Acc,2);
fprintf('\n The gravity Acceleration magnitude is acc = %1.4E [m s^-2]\n',A);