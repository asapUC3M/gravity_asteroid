function Asteroid = ImportAsteroid(FileName)

KmToM = 1000; %Conversion from Km to m

Aux = importdata(FileName);

L = length(Aux.rowheaders); %Number of faces + number of vertices, length of the file

Asteroid.Vertex.Counter = 0; %Counter of the number of vertices
Asteroid.Faces.Counter = 0;  %Counter of the number of faces

for i = 1 : L
   if Aux.rowheaders{i} == 'v' %If vertex is found
       Asteroid.Vertex.Counter = Asteroid.Vertex.Counter+1; %Vertex Number
       Asteroid.Vertex.Loc(Asteroid.Vertex.Counter,:) = Aux.data(i,:)*KmToM; %Location. Note a matrix with row index as the vertex number
   elseif Aux.rowheaders{i} == 'f'
       Asteroid.Faces.Counter = Asteroid.Faces.Counter +1;
       Asteroid.Faces.Order(Asteroid.Faces.Counter,:) = Aux.data(i,:); %Vertex linkage. Note a matrix of integers referring to vertex number. Row index is the face number
   end
end

end

