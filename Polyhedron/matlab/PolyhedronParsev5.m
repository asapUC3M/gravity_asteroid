function [EE,FF,E] = PolyhedronParsev5( Asteroid )

%{

    Universidad Carlos III de Madrid
    Author: Diego Garc�a Pardo

    Function Description:
    
    The following Function parses a structured file called Asteroid in
    order to produce the Dyads Corresponding to each edge as well as all
    normal to face vectors.

    First of all it must understood the meaning of the indices here used.
    
    EdgeLabel is the index used in E. It is known the number of edges in a
    polyhedron by Euler Formula for polyhedron. Therefore edges are
    labelled with a number going from 1 to NumberOfEdges. (Only ONE index
    per edge)

    For the face number it is used a counter simply.
       

    The output is E and F where:

---------- E is a structured variable as:
        -- E(EdgeLabel).Matrix --> The corresponding Dyad to Edge --> EdgeLabel
        
        -- E(EdgeLabel).vector1.start --> Start Position of the first vector linking 2 vertices
        -- E(EdgeLabel).vector1.end   --> End Position of the first vector linking 2 vertices
        -- E(EdgeLabel).vector2.start --> Start Position of the second vector linking 2 vertices
        -- E(EdgeLabel).vector2.end   --> End Position of the second vector linking 2 vertices
        
        In fact vector 1 and 2 share vertices, the direction of the vector
        is the only difference among them

        -- E(EdgeLabel).FaceNumber1   --> Face Number of the first vector found linking the 2 vertices
        -- E(EdgeLabel).FaceNumber2   --> Face Number of the second vector found linking the 2 vertices

---------- F is a cell variable such that:
        -- F{i} --> Outer product of the normal vector in face number i

%}


NumberOfEdges = Asteroid.Faces.Counter+Asteroid.Vertex.Counter-2; %Euler Formula For Edges

EdgeLabel = 1;                                      % Edge Counter Start
Link_Check = nan(NumberOfEdges);                    % Matrix to store whether it is the first or the second time in going through an edge
% In Link_Check it is stored the edge label assigned to each edge
Normal = nan(Asteroid.Faces.Counter,3);             % Normal to Face i is given by Normal(i,:) (as a row vector)

FF = nan(3,Asteroid.Faces.Counter);                 % Cell Matrix where for each F{i} the outer product of the normals is stored
EE = nan(3,NumberOfEdges);

ct = 1;
for i = 1 : Asteroid.Faces.Counter
    
    %% 1) Label Edges with numbers and save information of the vectors
    
    for j = 1 : 2 %Only triangles are considered
        
        Link     = sort([Asteroid.Faces.Order(i,j), Asteroid.Faces.Order(i,j+1)]); % Sort the edge number so it is the same independently of the direction taken
        Link_aux =      [Asteroid.Faces.Order(i,j), Asteroid.Faces.Order(i,j+1)];  % To save the order of the points which is important in computing the normals
        
        if isnan(Link_Check(Link(1),Link(2))) == 1 %If nan it is the first pass through an edge
            
            E(EdgeLabel).vector1.start   = Asteroid.Vertex.Loc(Link_aux(1),:)';%#ok  %Note the use of "Link_aux" (ordered) instead of "Link" (sorted)
            E(EdgeLabel).vector1.end     = Asteroid.Vertex.Loc(Link_aux(2),:)';%#ok  %Note the use of "Link_aux" (ordered) instead of "Link" (sorted)
            E(EdgeLabel).FaceNumber1     = i;%#ok                                    %To store the face that is being analyzing (In order to know what normal to use later)
            
            Link_Check(Link(1),Link(2)) = EdgeLabel;                           %Save the label assigned to this vector (as this is the first time)
            EdgeLabel = EdgeLabel+1;                                           %Increase EdgeLabel counter for future parsings
            
            
        elseif isfinite(Link_Check(Link(1),Link(2))) == 1  % If it is the second time Coming through this edge
            
            E(Link_Check(Link(1),Link(2))).vector2.start   = Asteroid.Vertex.Loc(Link_aux(1),:)';%#ok 
            E(Link_Check(Link(1),Link(2))).vector2.end     = Asteroid.Vertex.Loc(Link_aux(2),:)';%#ok 
            E(Link_Check(Link(1),Link(2))).FaceNumber2     = i;%#ok 
            
        end
        
    end
    
    % Last Point is An Exception as the last vector goes from 3 to 1,
    % therefore it is outside the loop of variable j.
    
    Link = sort([Asteroid.Faces.Order(i,3) Asteroid.Faces.Order(i,1)]);
    Link_aux =      [Asteroid.Faces.Order(i,3), Asteroid.Faces.Order(i,1)];
    
    if isnan(Link_Check(Link(1),Link(2))) == 1
        
        E(EdgeLabel).vector1.start   = Asteroid.Vertex.Loc(Link_aux(1),:)';%#ok 
        E(EdgeLabel).vector1.end     = Asteroid.Vertex.Loc(Link_aux(2),:)';%#ok 
        E(EdgeLabel).FaceNumber1     = i;%#ok 
        
        Link_Check(Link(1),Link(2)) = EdgeLabel;
        EdgeLabel = EdgeLabel+1;
        
    elseif isfinite(Link_Check(Link(1),Link(2))) == 1 %If it is the second time coming through this edge
        
        E(Link_Check(Link(1),Link(2))).vector2.start   = Asteroid.Vertex.Loc(Link_aux(1),:)';%#ok 
        E(Link_Check(Link(1),Link(2))).vector2.end     = Asteroid.Vertex.Loc(Link_aux(2),:)';%#ok 
        E(Link_Check(Link(1),Link(2))).FaceNumber2     = i;%#ok 
        
    end
    
    
    %% 2) Compute Normal To Face
    
    %%% Edge Vectors, first 2 are selected
    V1 = Asteroid.Vertex.Loc(Asteroid.Faces.Order(i,2),:)-Asteroid.Vertex.Loc(Asteroid.Faces.Order(i,1),:);
    V2 = Asteroid.Vertex.Loc(Asteroid.Faces.Order(i,3),:)-Asteroid.Vertex.Loc(Asteroid.Faces.Order(i,2),:);
    %Only two vectors are used to compute the normal to Face
    Normal(i,:) = cross(V1,V2);
    Normal(i,:) = Normal(i,:)/norm(Normal(i,:),2); %Make unitary
    
    FF(1:3,ct:1:(ct+2)) = Normal(i,:)'*Normal(i,:);
    ct = ct+3;
end

%% Compute dyad's
if length(E) == NumberOfEdges              % Checks if the dyads are Symmetric (First Check)
    ct = 1;
    for i = 1 : NumberOfEdges
        
        NN1 = Normal(E(i).FaceNumber1,:); %Normal to face of the first found face sharing edge
        NN2 = Normal(E(i).FaceNumber2,:); %Normal to face of the second found face sharing edge
        
        NE1 = cross(E(i).vector1.end-E(i).vector1.start,NN1); %Normal to edge in face 1
        NE1 = NE1/norm(NE1,2);                                %Unitary Normal
        NE2 = cross(E(i).vector2.end-E(i).vector2.start,NN2); %Normal to edge in face 2
        NE2 = NE2/norm(NE2,2);                                %Unitary Normal
        
        E(i).Matrix = NN1'*NE1 + NN2'*NE2;%#ok                  %Dyad at edge i
        EE(1:3,ct:1:(ct+2)) = E(i).Matrix;                    %Matrix Allocation for future matrix products
        ct = ct+3;
        
    end
       
else
    
    display('Error in Parsing Edges');
    disp(length(E))
    EE = nan;
    FF = nan;
    
end



end

