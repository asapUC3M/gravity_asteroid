function check_val = Check_Symmetry( E,F )

check_val(1) = 1; % Good for E
check_val(2) = 1; % Good for F

for k = 1 : length(E)
    
    [h,w] = size(E(k).Matrix);
    
    for i = 1 : h
        for j = 1 : w
            
            
            if abs(E(k).Matrix(i,j) - E(k).Matrix(j,i))>1e-8
                
                check_val(1) = 0;
                disp(['Non Symmetry Found in Dyad E ' num2str([k,i,j])]);
                return;
                
            end
            
            
  
        end
    end
    
end

for k = 1 : length(F)
    
    [h,w] = size(F{k});
    
        for i = 1 : h
            for j = 1 : w
                
                    if abs(F{k}(i,j) - F{k}(j,i))> 1e-8
                        
                        check_val(2) = 0;
                        disp('Non Symmetry Found in Dyad F');
                        return;
                        
                    end
                    
            end
            
        end
end