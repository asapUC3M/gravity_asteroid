function AsteroidPlotter(Asteroid)

%{
    Asteroid Is a Structured Variable as:
        Asteroid.Vertex
        Asteroid.Faces

    Then for vertex the following tree opens:
        .Vertex.Counter --> How many Vertex there are
        .Vertex.Pos     --> When file was parsed, at what position was
        found
        .Vertex.Loc     --> Localization of a vertex in 3D space
                            Note: The coordinate frame is not well defined

    For the Faces the following tree opens:
        .Faces.Counter  --> How many faces we have
        .Faces.Order    --> indicates the calling order to vertex already
                            indexed

%}

h = patch('vertices',Asteroid.Vertex.Loc,'faces',Asteroid.Faces.Order,'facecolor','b');

end

