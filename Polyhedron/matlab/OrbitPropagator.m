clc
clear all
close all

S = 2.1e3;              % Kg/m^3;        Mean Density
G = 6.67384e-11;        % m^3/Kg/s^2     Gravitational Constant
GS = G*S;               % Product Density x Gravity Constant s^(-2)            
w_Asteroid = 2*pi/(4.095 * 3600); %Castalia rotation info from wikipedia

Asteroid = ImportAsteroid('Castalia.txt');      % Function to process the geometry into a structured variable
[EE,FF,E] = PolyhedronParsev5(Asteroid);            % This function Generates the Dyad's E and F from the structured developed in "ImportAsteroid"

PosVector = max(Asteroid.Vertex.Loc)';                           % Position in Space where to evaluate U and Acc

tspan = [0 5e4];
InitState = [PosVector;[0.1;0;0]];
opt = odeset('RelTol',1e-8,'AbsTol',1e-8);

tic
[t,X] = ode113(@FieldToIntegrator,tspan,InitState,opt,GS,EE,FF,E,Asteroid,w_Asteroid,tspan(2));
toc
plot3(X(:,1),X(:,2),X(:,3))
hold on
plot3(X(end,1),X(end,2),X(end,3),'r+');
AsteroidPlotter(Asteroid)