function Out = Acceleration(PosVector,GS,EE,FF,E,Asteroid)

[RRe,RRf] = Aux_Vectors(PosVector, E, Asteroid);

S_E = EE * RRe;
S_F = FF * RRf;

Acc = GS * (-S_E + S_F);

Out = Acc;

end

