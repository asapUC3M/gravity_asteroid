function [RRe,RRf] = Aux_Vectors(PosVector, E, Asteroid )

E_length = Asteroid.Faces.Counter + Asteroid.Vertex.Counter-2;
wf = SolidAngle_wf(PosVector,Asteroid);

RRf = nan(Asteroid.Faces.Counter*3,1);      % Times 3 because vectors have 3 elements
RRe = nan(E_length*3,1);                    % Times 3 because vectors have 3 elements
ct = 1;

for i = 1 : Asteroid.Faces.Counter
   
    RRf(ct:1:(ct+2),1) = (Asteroid.Vertex.Loc(Asteroid.Faces.Order(i,1),:)' - PosVector)*wf(i); %Vector 1 Start Location is arbitrary
    
    re = E(i).vector2.start - PosVector; %Vector 1 Start Location is arbitrary
    
    a = norm(E(i).vector2.start - PosVector,2);
    b = norm(E(i).vector2.end   - PosVector,2);
    e = norm(E(i).vector2.end   - E(i).vector2.start,2);
    
    if (a+b-e) == 0
        Le = 0;
    else
        Le = log((a+b+e)/(a+b-e));
    end
    
    RRe(ct:1:(ct+2),1) = re*Le;
    ct = ct+3;
    
end


for i = (Asteroid.Faces.Counter+1) : E_length
   
    re = E(i).vector2.start - PosVector; %Vector 1 Start Location is arbitrary
    
    a = norm(E(i).vector2.start-PosVector,2);
    b = norm(E(i).vector2.end - PosVector,2);
    e = norm(E(i).vector2.end-E(i).vector2.start,2);
    
    if (a+b-e) == 0
        Le = 0;
    else
        Le = log((a+b+e)/(a+b-e));
    end
    
    RRe(ct:1:(ct+2),1) = re*Le;
    ct = ct+3;
    
end



end

