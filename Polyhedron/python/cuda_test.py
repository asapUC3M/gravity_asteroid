#!/usr/bin/python
# -*- coding: utf-8 -*-

import numpy
import time
from math import e, log as ln, pi
from sys import argv
import pycuda.autoinit
from pycuda.compiler import SourceModule
import pycuda.driver as drv
import pycuda.gpuarray as gpuarray

A = gpuarray.to_gpu(numpy.array(map(float, range(8))).astype(numpy.float32))
B = gpuarray.to_gpu(numpy.array([10.0]*8).astype(numpy.float32))
C = gpuarray.to_gpu(numpy.array([0.0]*8).astype(numpy.float32))
#D = gpuarray.to_gpu(numpy.array([[[1, 0], [-1, 2]], [[6, 7], [8, 9]]]).astype(numpy.float32))
D = gpuarray.to_gpu(numpy.array([[[6, 7], [8, 9]], [[.6, .7], [.8, .9]]]).astype(numpy.float32))
print "A = %s" % A
print "B = %s" % B
print "C = %s" % C
test_kernel = SourceModule ( """
__global__ void add(float *A, float *B, float *C, float *D, int stride)
{
    int n = threadIdx.x + blockIdx.x*blockDim.x;
    C[n] = A[n] + B[n];
    float E[2][2];
    E[0][0] = 10.0;
    E[0][1] = 20.0;
    E[1][0] = 30.0;
    E[1][1] = 40.0;   
    if ((n>0)&&(n<5)) {
        C[n] = D[n - 1];
    } else if (n==0) {
        C[n] = E[1][0];
    }
}
""" )
add = test_kernel.get_function('add')
add(A, B, C, D, numpy.int32(D.strides[0]), block=(4, 1, 1), grid =(2, 1))
print "A = %s" % A
print "B = %s" % B
print "C = %s" % gpuarray.sum(C)
print "D = %s" % D