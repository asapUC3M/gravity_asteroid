#!/usr/bin/python
# -*- coding: utf-8 -*-
# Daniel González Arribas (dangonza@ing.uc3m.es)

import numpy
import time
from math import e, log as ln, pi
from sys import argv
try:
    import pycuda.autoinit
    from pycuda.compiler import SourceModule
    import pycuda.driver as drv
    import pycuda.gpuarray as gpuarray
except:
    CUDA_AVAILABLE = False
else:
    CUDA_AVAILABLE = True

G = 6.67384e-11

class SolidAngleIsZeroException(Exception): pass
class BadArgumentsException(Exception): pass
class CUDANotAvailableException(Exception): pass

def sign(a):
    if a < 0:
        return -1
    else:
        return 1

class Dyad(object): 
    """ 
    3x3 Dyad object 
    """
    def __init__(self, a, b=0):
        """ 
        For two arguments, initializes the dyad as the tensor product of them
        For one argument, initializes the dyad as the given matrix
        """
        if b:
            self.m = numpy.matrix([(b*a.v[0]).v, (b*a.v[1]).v, (b*a.v[2]).v])
        else:
            self.m = a
    def __repr__(self):
        return "Dyad(%s)" % repr(self.m)
    def __mul__(self, other):
        b = numpy.matrix(other.v).transpose()
        if other.N != 3:
            raise Exception, ""
        else:
            return Vector(self.m*b)
    def __add__(self, other):
        return Dyad(self.m + other.m)
        
class Vector(object):
    """ 
    A simple vector implementation 
    """
    def __init__(self, v, N=3):
        self.v = []
        for a in v:
            try:
                self.v.append(a[0, 0])
            except:
                self.v.append(a)
        self.N = N
        if len(v) != N:
            raise Exception, "Not a vector with the required dimensions"
    def norm(self, L=2):
        """
        Returns the lp norm of the vector
        
        Parameters
        ----------
        p : lp exponent, optional
        Default value is p = 2 (euclidean norm)
        """
        return (sum(map(lambda x: abs(x)**L, self.v)))**(1.0/L)
    def __repr__(self):
        return "Vector(%s)" % repr(self.v)
    def __get__(self, i):
        return self.v[i]
    def __set__(self, i, value):
        self.v[i] = value
    def __add__(self, other):
        """
        Standard vector addition
        """
        if self.N != other.N:
            raise Exception, "Cannot add vectors"
        else:
            v = []
            for i in range(self.N):
                v.append(self.v[i] + other.v[i])
            return Vector(v, self.N)
    def __sub__(self, other):
        """
        Standard vector substraction
        """
        if self.N != other.N:
            raise Exception, "Cannot add vectors"
        else:
            v = []
            for i in range(self.N):
                v.append(self.v[i] - other.v[i])
            return Vector(v, self.N)
    def __mul__(self, other):
        """
        Scalar product
        """
        try:
            res = numpy.dot(self.v, other.v)
        except:
            res = Vector(map(lambda x: x*other, self.v), self.N)
        try:
            return res[0,0]
        except:
            return res
    def __xor__(self, other):
        """ 
        Cross-product 
        """
        if self.N != other.N:
            raise Exception, "Cannot multiply vectors"
        elif self.N not in [3]:
            raise Exception, "Cross-product not implemented for N > 3"
        elif self.N == 3:
            #v = []
            #for i in range(self.N):
                #v.append((self.v[(i + 1) % 3]*other.v[(i + 2) % 3] - self.v[(i + 2) % 3]*other.v[(i + 1) % 3]))
            return Vector(numpy.cross(self.v, other.v), self.N)
    def __eq__(self, other):
        eql = True
        try:
            for i in range(self.N):
                eql = eql and (self.v[i] == other.v[i])
        except:
            eql = False
        return eql
    def to_onb(self, onb):
        """ 
        Returns a representation of the vector in the coordinate system given by onb
        
        Parameters:
        -----------
        onb : the orthonormal basis, should be a list of 3 Vector-like objects
        """
        QT = numpy.matrix([q.v for q in onb])
        v = QT*(numpy.matrix(self.v).transpose())
        return Vector(v, self.N)
    def normalize(self, L=2):
        if self.norm(L) == 0:
            print self
            raise Exception
        return self*(1/self.norm(L))
    def to_txt(self):
        return repr(self.v)
        
class Asteroid(object):
    def __init__(self, x, y, z, indexes, density):
        def in_edges(edge):
            return (edge in self.iedges) or (edge[::-1] in self.iedges)
        self.x = x
        self.y = y
        self.z = z
        self.p = [Vector(v) for v in zip(x, y, z)]
        self.ifaces = indexes
        self.faces = []
        self.d = density
        self.iedges = []
        self.edges = []
        self.normals = []
        self.Ee = []
        self.Ff = []
        #self.qref = []
        for f in self.ifaces:
            edge1 = (f[0], f[1])
            edge2 = (f[1], f[2])
            edge3 = (f[2], f[0])
            for edge in (edge1, edge2, edge3):
                if not in_edges(edge):
                    self.iedges.append(edge)
        for a, b in self.iedges:
            u = Vector([self.x[a], self.y[a], self.z[a]])
            v = Vector([self.x[b], self.y[b], self.z[b]])
            self.edges.append([u, v])
        for a, b, c in self.ifaces:
            u = Vector([self.x[a], self.y[a], self.z[a]])
            v = Vector([self.x[b], self.y[b], self.z[b]])
            w = Vector([self.x[c], self.y[c], self.z[c]])
            self.faces.append([u, v, w])
        self.nfaces = len(self.faces)
        self.nedges = len(self.edges)
        print "n vertex", len(self.p)
        print "n faces", self.nfaces
        print "n edges", self.nedges
        # Cálculo del signo de la primera normal
        f = self.faces[0]
        u = (f[1] - f[0])
        v = (f[2] - f[1])
        w = u^v 
        try:
            a = w*(f[0] - Vector([0,0,0]))
            self.sign = a/abs(a)
            assert not self.sign
        except:
            # In the rare case that f[0] == Vector([0,0,0])
            a = w*(f[0] - Vector([0.1,0.1,0.1]))
            self.sign = a/abs(a)
        self.compute_normals()
        for i in range(self.nedges):
            self.Ee.append(self.compute_Ee(i))
        for i in range(self.nfaces):
            self.Ff.append(self.compute_Ff(i))
            #self.qref.append(self.compute_qref(i))
    def compute_normals(self):
        self.face_normals = []
        for i in range(self.nfaces):
            f = self.faces[i]
            u = (f[1] - f[0])
            v = (f[2] - f[1])
            w = u^v
            self.face_normals.append(w.normalize()*self.sign)
    def Le(self, p, i):
        if i >= self.nedges:
            raise Exception, "Non-existant edge"
        else:
            a = (p - self.edges[i][0]).norm()
            b = (p - self.edges[i][1]).norm()
            le = (self.edges[i][0] - self.edges[i][1]).norm()
            try:
                return ln((a + b + le)/(a + b - le))
            except ZeroDivisionError:
                return 0 # re·Ee·re = 0 in this case
            except:
                print a, b, le
                print p
                print self.edges[i]
                raise
    def nf(self, i):
        return self.face_normals[i]
    def re(self, p, n):
        re0 = (self.edges[n][0] - p) 
        if re0.norm():
            return re0
        else:
            return (self.edges[n][1] - p)
    def rf(self, p, n):
        return (self.faces[n][0] - p) 
    def wf(self, p, n):
        #onb = self.qref(n)
        face = self.faces[n]
        r0 = (face[0] - p) #.to_onb(onb)
        r1 = (face[1] - p)#.to_onb(onb)
        r2 = (face[2] - p)#.to_onb(onb)
        n0 = r0.norm()
        n1 = r1.norm()
        n2 = r2.norm()
        num = r0*(r1^r2)
        den = n1*n2*n0 + (r0*r1)*n2 + (r1*r2)*n0 + (r2*r0)*n1
        return 2*numpy.arctan2(num, den) # eq. 27
    def deltaZ(self, p, n):
        return self.nf(n)*(self.faces[n][0] - p)
    def compute_Ff(self, i):
        n = self.nf(i)
        return Dyad(n, n)
    def check_face_edge(self, nf, ne):
        iedge = self.iedges[ne]
        iface = self.ifaces[nf]
        return (iedge[0] in iface) and (iedge[1] in iface)
    def compute_Ee(self, n):
        edge = self.edges[n]
        faces = []
        ifs = []
        for i in range(self.nfaces):
            if self.check_face_edge(i, n):
                faces.append(self.faces[i])
                ifs.append(i)
        nA = self.nf(ifs[0])
        nB = self.nf(ifs[1])
        vedge = (edge[1] - edge[0]).normalize()
        for i in range(3):
            if faces[0][i] not in edge:
                if faces[0][(1 + i) % 3] == edge[0]:
                    nA12 = (nA^vedge)*(-1)
                    nB21 = nB^vedge
                else:
                    nA12 = nA^vedge
                    nB21 = (nB^vedge)*(-1)
        return (Dyad(nA, nA12) + Dyad(nB, nB21))
    def U(self, p):
        re = []
        rf = []
        t0 = time.time()
        for i in range(self.nedges):
            re.append(self.re(p, i))
        t1 = time.time()
        for i in range(self.nfaces):
            rf.append(self.rf(p, i))
        t2 = time.time()
        A = 0.0
        for i in range(self.nedges):
            cont = (re[i]*(self.Ee[i]*re[i])*self.Le(p, i))
            A += cont
        t3 = time.time()
        B = 0.0
        for i in range(self.nfaces):
            cont = (rf[i]*(self.Ff[i]*rf[i])*self.wf(p, i))
            B += cont
        t4 = time.time()
        print u"re: %ss" % (t1 - t0)
        print u"rf: %ss" % (t2 - t1)
        print u"Te: %ss" % (t3 - t2)
        print u"Tf: %ss" % (t4 - t3)
        return - ((A - B)*G*self.d*0.5)
    def g(self, p):
        re = []
        rf = []
        for i in range(self.nedges):
            re.append(self.re(p, i))
        for i in range(self.nfaces):
            rf.append(self.rf(p, i))
        A = Vector([0.0, 0.0, 0.0])
        for i in range(self.nedges):
            A += (self.Ee[i]*re[i])*self.Le(p, i)
        B = Vector([0.0, 0.0, 0.0])
        for i in range(self.nfaces):
            B += (self.Ff[i]*rf[i])*self.wf(p, i)
        return ((A*(-1) + B)*G*self.d)

class AsteroidCUDA(Asteroid):
    def __init__(self, x, y, z, indexes, density):
        from cuda_kernels import *
        if not CUDA_AVAILABLE:
            raise CUDANotAvailableException
        max_threads_per_block = 256 #512
        Asteroid.__init__(self, x, y, z, indexes, density)
        self.n_edges = len(self.edges)
        self.n_faces = len(self.faces)
        zero_dyad = Dyad(numpy.matrix([[0, 0, 0], [0, 0, 0], [0, 0, 0]]))
        zero_edge = [Vector([0, 0, 0]), Vector([0, 0, 0])]
        zero_face = [Vector([0, 0, 0]), Vector([0, 0, 0]), Vector([0, 0, 0])]
        self.n_blocks_faces = self.n_faces/max_threads_per_block + 1
        self.n_blocks_edges = self.n_edges/max_threads_per_block + 1
        self.extra_zero_faces = self.n_blocks_faces*max_threads_per_block - self.n_faces
        self.extra_zero_edges = self.n_blocks_edges*max_threads_per_block - self.n_edges
        assert (self.extra_zero_edges > 0) and (self.extra_zero_faces > 0)
        self.Ee_gpu = gpuarray.to_gpu(numpy.array([E.m for E in (self.Ee+[zero_dyad]*self.extra_zero_edges)]).astype(numpy.float32))
        self.Ff_gpu = gpuarray.to_gpu(numpy.array([F.m for F in (self.Ff+[zero_dyad]*self.extra_zero_faces)]).astype(numpy.float32))
        self.edges_gpu = gpuarray.to_gpu(numpy.array([[v.v for v in edge] for edge in (self.edges+[zero_edge]*self.extra_zero_edges)]).astype(numpy.float32))
        self.faces_gpu = gpuarray.to_gpu(numpy.array([[v.v for v in face] for face in (self.faces+[zero_face]*self.extra_zero_faces)]).astype(numpy.float32))
        self.max_threads_per_block = max_threads_per_block
        self.kernel_U_edges = SourceModule(lib + kernel_U_edges)
        self.kernel_g_edges = SourceModule(lib + kernel_g_edges)
        self.kernel_U_faces = SourceModule(lib + kernel_U_faces)
        self.kernel_g_faces = SourceModule(lib + kernel_g_faces)
        self.edge_cont = self.kernel_U_edges.get_function('edge_cont')
        self.face_cont = self.kernel_U_faces.get_function('face_cont')
        self.edge_cont_g = self.kernel_g_edges.get_function('edge_cont')
        self.face_cont_g = self.kernel_g_faces.get_function('face_cont')  
    def re(self, p, n):
        re0 = (self.edges[n][0] - p) #.to_onb(self.qref(n))
        if re0.norm():
            return re0
        else:
            return (self.edges[n][1] - p)
    def rf(self, p, n):
        return (self.faces[n][0] - p) # .to_onb(self.qref(n))
    def U(self, p):
        re = []
        rf = []
        #t0 = time.time() 
        A_gpu = gpuarray.to_gpu(numpy.zeros(len(self.edges)).astype(numpy.float32))        
        p_gpu = gpuarray.to_gpu(numpy.array(p.v).astype(numpy.float32))
        t0 = time.time() 
        self.edge_cont(p_gpu, self.Ee_gpu, self.edges_gpu, A_gpu, block=(self.max_threads_per_block, 1, 1), grid =(self.n_blocks_edges, 1))
        A = gpuarray.sum(A_gpu)
        t3 = time.time()
        B_gpu = gpuarray.to_gpu(numpy.zeros(len(self.faces)).astype(numpy.float32))
        p_gpu = gpuarray.to_gpu(numpy.array(p.v).astype(numpy.float32))
        self.face_cont(p_gpu, self.Ff_gpu, self.faces_gpu, B_gpu, block=(self.max_threads_per_block, 1, 1), grid =(self.n_blocks_faces, 1))
        B = gpuarray.sum(B_gpu)
        t4 = time.time()
        print "CUDA timings:"
        print u"Te: %ss" % (t3 - t0)
        print u"Tf: %ss" % (t4 - t3)
        return - ((A - B)*G*self.d*0.5)
    def g(self, p):
        re = []
        rf = []
        t0 = time.time()
        Ax_gpu = gpuarray.to_gpu(numpy.zeros(len(self.edges)).astype(numpy.float32))   
        Ay_gpu = gpuarray.to_gpu(numpy.zeros(len(self.edges)).astype(numpy.float32))        
        Az_gpu = gpuarray.to_gpu(numpy.zeros(len(self.edges)).astype(numpy.float32))        
        p_gpu = gpuarray.to_gpu(numpy.array(p.v).astype(numpy.float32))
        self.edge_cont_g(p_gpu, self.Ee_gpu, self.edges_gpu, Ax_gpu, Ay_gpu, Az_gpu, 
            block=(self.max_threads_per_block, 1, 1), grid =(self.n_blocks_edges, 1))
        Ax = gpuarray.sum(Ax_gpu).get()
        Ay = gpuarray.sum(Ay_gpu).get()
        Az = gpuarray.sum(Az_gpu).get()
        t3 = time.time()
        Bx_gpu = gpuarray.to_gpu(numpy.zeros(len(self.faces)).astype(numpy.float32))   
        By_gpu = gpuarray.to_gpu(numpy.zeros(len(self.faces)).astype(numpy.float32))        
        Bz_gpu = gpuarray.to_gpu(numpy.zeros(len(self.faces)).astype(numpy.float32))   
        p_gpu = gpuarray.to_gpu(numpy.array(p.v).astype(numpy.float32))
        self.face_cont_g(p_gpu, self.Ff_gpu, self.faces_gpu, Bx_gpu, By_gpu, Bz_gpu, 
            block=(self.max_threads_per_block, 1, 1), grid =(self.n_blocks_faces, 1))
        Bx = gpuarray.sum(Bx_gpu).get()
        By = gpuarray.sum(By_gpu).get()
        Bz = gpuarray.sum(Bz_gpu).get()
        t4 = time.time()
        print "CUDA timings:"
        print u"Te: %ss" % (t3 - t0)
        print u"Tf: %ss" % (t4 - t3)
        return (Vector(numpy.array([Ax, Ay, Az]))*(-1) + Vector(numpy.array([Bx, By, Bz])))*G*self.d
                
def fs(s):
    return filter(lambda x: x, s.strip().split(' '))

def readmesh(f):
    """
    Reads and processes a mesh from an .obj file
    
    Parameters:
    -----------
    f : a file object
    """
    lines = [l.strip() for l in f.readlines()]
    x = []
    y = []
    z = []
    indexes = []
    for l in lines:
        if l.startswith("v"):
            nums = map(float, fs(l[2:]))
            x.append(nums[0]*1000)
            y.append(nums[1]*1000)
            z.append(nums[2]*1000)
        elif l.startswith("f"):
            nums = map(lambda x: int(x) - 1, fs(l[2:]))
            indexes.append(nums)
    return (x, y, z, indexes)

def string_2_vector(s):
    return Vector(map(float, s.split(',')))

if __name__ == "__main__":
    assert (len(argv)/2)*2 == len(argv) # assert len(argv) is even
    fname = argv[1]
    remaining_args = argv[2:]
    density = 1
    mode = ''
    u_list = []
    g_list = []
    while remaining_args:
        arg = remaining_args.pop(0)
        if arg in ['-d', '--density']:
            density = float(remaining_args.pop(0))
        elif arg in ['-u', '--potential']:
            while remaining_args and not remaining_args[0].startswith('-'):
                u_list.append(string_2_vector(remaining_args.pop(0)))
        elif arg in ['-g', '--gravity']:
            while remaining_args and not remaining_args[0].startswith('-'):
                g_list.append(string_2_vector(remaining_args.pop(0)))
        else:
            raise BadArgumentsException, "The argument chain was not formed correctly"
    t0 = time.time()
    with open(fname) as f:
        (x, y, z, indexes) = readmesh(f)
    tf = time.time()
    print "Mesh processing took %2.2f seconds" % (tf - t0)
    t0 = time.time()
    ast = Asteroid(x, y, z, indexes, density)
    CUDAast = AsteroidCUDA(x, y, z, indexes, density)
    tf = time.time()
    print "Initialization took %2.2f seconds" % (tf - t0)
    t0 = time.time()
    for p in u_list:
        print u'Potential at %s: %s' % (p.to_txt(), ast.U(p))
        print u'Potential at %s: %s (CUDA)' % (p.to_txt(), CUDAast.U(p))
    for p in g_list:
        print u'Attraction at %s: %s' % (p.to_txt(), ast.g(p).to_txt())
        print u'Attraction at %s: %s (CUDA)' % (p.to_txt(), CUDAast.g(p).to_txt())
    tf = time.time()
    print "Remaining computations took %.2f seconds" % (tf -t0)
