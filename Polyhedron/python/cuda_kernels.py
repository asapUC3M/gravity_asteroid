#!/usr/bin/python
# -*- coding: utf-8 -*-
# (c) Daniel González Arribas (dangonza@ing.uc3m.es)
# 

lib = """
    __device__ void norm3(float *vector, float *res) 
    {
        *res = sqrt(vector[1]*vector[1] + vector[2]*vector[2] + vector[0]*vector[0]);
    } 
    __device__ void subs3(float *v1, float *v2, float *result) 
    {
        result[0] = v1[0] - v2[0];
        result[1] = v1[1] - v2[1];
        result[2] = v1[2] - v2[2];
    } 
    __device__ void dot3(float *v1, float *v2, float *result) 
    {
        *result = 0;
        for (int i=0;i<3;i++){
            *result += v1[i]*v2[i];
        } 
    }         
    __device__ void cross3(float *v1, float *v2, float *result) 
    {
        for (int i=0;i<3;i++){
            result[i] = v1[(i+1)%3]*v2[(i+2)%3] - v1[(i+2)%3]*v2[(i+1)%3];
        } 
    } 
    __device__ void Mv3(float M[3][3], float *v, float *result) 
    {
        for (int i=0;i<3;i++) {
            result[i] = 0;
            for (int j=0;j<3;j++) {
                result[i] += M[i][j]*v[j];
            }
        }
    } 
    __device__ void extract_edge(float *edges, float *edge_p, int n, int m) 
    {
        for (int i=0;i<3;i++) {
            edge_p[i] = edges[6*n+3*m+i];
        }
    } 
    __device__ void extract_face(float *faces, float *face_p, int n, int m) 
    {
        for (int i=0;i<3;i++) {
            face_p[i] = faces[9*n+3*m+i];
        }
    } 
    __device__ void extract_dyad(float *Ee, float M[3][3], int n) 
    {
        for (int i=0;i<3;i++) {
            for (int j=0;j<3;j++) {
                M[i][j] = Ee[9*n + 3*i + j];
            }
        }
    } 
    """

kernel_g_faces = """
__global__ void face_cont(float *p, float *Ff, float *faces, float *c_x, float *c_y, float *c_z)
{
    const unsigned int n = threadIdx.x + blockIdx.x*blockDim.x;
    float face_p0[3];
    float face_p1[3];
    float face_p2[3];
    float rf[3];
    extract_face(faces, face_p0, n, 0);
    extract_face(faces, face_p1, n, 1);
    extract_face(faces, face_p2, n, 2);
    subs3(face_p0, p, rf);
    if ((rf[0]==0) && (rf[1]==0) && (rf[2]==0)) { 
        subs3(face_p1, p, rf);
    }
    /* ------- Calculation of w_f ----------- */      
    float r0[3];
    float r1[3];
    float r2[3];
    float n0;
    float n1;
    float n2;
    float t0;
    float t1;
    float t2;
    float wf;
    float denominator;
    float cr[3];
    float numerator;
    subs3(face_p0, p, r0);
    subs3(face_p1, p, r1);
    subs3(face_p2, p, r2);
    norm3(r0, &n0);
    norm3(r1, &n1);
    norm3(r2, &n2);
    cross3(r1, r2, cr);
    dot3(r0, cr, &numerator);
    dot3(r0, r1, &t0);
    dot3(r1, r2, &t1);
    dot3(r2, r0, &t2);
    denominator = n1*n2*n0 + t0*n2 + t1*n0 + t2*n1;
    wf = 2*atan2(numerator, denominator);
    /* ------- Calculation of the product ----------- */ 
    float Ffn_rf[3];
    float Ffn[3][3];
    extract_dyad(Ff, Ffn, n);
    Mv3(Ffn, rf, Ffn_rf);
    c_x[n] = wf*Ffn_rf[0];
    c_y[n] = wf*Ffn_rf[1];
    c_z[n] = wf*Ffn_rf[2];
}
"""
        
kernel_U_edges =  """
__global__ void edge_cont(float *p, float *Ee, float *edges, float *result)
{
    const unsigned int n = threadIdx.x + blockIdx.x*blockDim.x;
    /* ------- Calculation of r_e ----------- */
    float re[3];
    float edge_p0[3];
    float edge_p1[3];
    extract_edge(edges, edge_p0, n, 0);
    extract_edge(edges, edge_p1, n, 1);
    subs3(edge_p0, p, re);
    if ((re[0]==0) && (re[1]==0) && (re[2]==0)) { 
        subs3(edge_p1, p, re);
    }
    /* ------- Calculation of L_e ----------- */
    float a[3];
    float b[3];
    float ed[3];
    float na;
    float nb;
    float le;
    float Le;
    subs3(p, edge_p0, a);
    subs3(p, edge_p1, b);
    subs3(edge_p0, edge_p1, ed);
    norm3(a, &na);
    norm3(b, &nb);
    norm3(ed, &le);
    if ((na + nb) == le) {
        Le = 0;
    } else {
        Le = log((na + nb + le)/(na + nb - le));
    }
    /* ----- Calculation of the product  ----- */
    float temp[3];
    float temp2;
    float Een[3][3];
    extract_dyad(Ee, Een, n);
    Mv3(Een, re, temp);
    dot3(re, temp, &temp2);
    result[n] = temp2*Le;  
}
            """
kernel_g_edges = """
__global__ void edge_cont(float *p, float *Ee, float *edges, float *c_x, float *c_y, float *c_z)
{
    const unsigned int n = threadIdx.x + blockIdx.x*blockDim.x;
    /* ------- Calculation of r_e ----------- */
    float re[3];
    float edge_p0[3];
    float edge_p1[3];
    extract_edge(edges, edge_p0, n, 0);
    extract_edge(edges, edge_p1, n, 1);
    subs3(edge_p0, p, re);
    if ((re[0]==0) && (re[1]==0) && (re[2]==0)) { 
        subs3(edge_p1, p, re);
    }
    /* ------- Calculation of L_e ----------- */
    float a[3];
    float b[3];
    float ed[3];
    float na;
    float nb;
    float le;
    float Le;
    subs3(p, edge_p0, a);
    subs3(p, edge_p1, b);
    subs3(edge_p0, edge_p1, ed);
    norm3(a, &na);
    norm3(b, &nb);
    norm3(ed, &le);
    if ((na + nb) == le) {
        Le = 0;
    } else {
        Le = log((na + nb + le)/(na + nb - le));
    }
    /* ----- Calculation of the product  ----- */
    float Een_re[3];
    float Een[3][3];
    extract_dyad(Ee, Een, n);
    Mv3(Een, re, Een_re);
    c_x[n] = Le*Een_re[0];
    c_y[n] = Le*Een_re[1];
    c_z[n] = Le*Een_re[2];
}
"""
            
kernel_U_faces =  """
__global__ void face_cont(float *p, float *Ff, float *faces, float *result)
{
    const unsigned int n = threadIdx.x + blockIdx.x*blockDim.x;
    float face_p0[3];
    float face_p1[3];
    float face_p2[3];
    float rf[3];
    extract_face(faces, face_p0, n, 0);
    extract_face(faces, face_p1, n, 1);
    extract_face(faces, face_p2, n, 2);
    subs3(face_p0, p, rf);
    if ((rf[0]==0) && (rf[1]==0) && (rf[2]==0)) { 
        subs3(face_p1, p, rf);
    }
    /* ------- Calculation of w_f ----------- */      
    float r0[3];
    float r1[3];
    float r2[3];
    float n0;
    float n1;
    float n2;
    float t0;
    float t1;
    float t2;
    float wf;
    float denominator;
    float cr[3];
    float numerator;
    subs3(face_p0, p, r0);
    subs3(face_p1, p, r1);
    subs3(face_p2, p, r2);
    norm3(r0, &n0);
    norm3(r1, &n1);
    norm3(r2, &n2);
    cross3(r1, r2, cr);
    dot3(r0, cr, &numerator);
    dot3(r0, r1, &t0);
    dot3(r1, r2, &t1);
    dot3(r2, r0, &t2);
    denominator = n1*n2*n0 + t0*n2 + t1*n0 + t2*n1;
    wf = 2*atan2(numerator, denominator);
    /* ------- Calculation of the product ----------- */ 
    float temp[3];
    float temp2;
    float Ffn[3][3];
    extract_dyad(Ff, Ffn, n);
    Mv3(Ffn, rf, temp);
    dot3(rf, temp, &temp2);
    result[n] = wf*temp2;   
}
"""