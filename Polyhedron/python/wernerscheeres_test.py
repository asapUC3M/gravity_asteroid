#!/usr/bin/python
# -*- coding: utf-8 -*-
# Daniel González Arribas (dangonza@ing.uc3m.es)

import numpy, time, pylab, pickle, scipy.interpolate
from mayavi.mlab import *
from math import e, log as ln, pi
from wernerscheeres import *

with open("testeroid2.txt", 'r') as f:
    (x, y, z, indexes) = readmesh(f)
testeroid = Asteroid(x, y, z, indexes, 10000000000.0)
testeroid.compute_normals()

if __name__ == "__main__":
	#for i in range(8):
		#print testeroid.qref(i)
	#print testeroid.face_normals
	r = range(0, 160)
	(a, b, c, d) = ([], [], [], [])
	try:
		f = open("gdata.pck", 'r')
		X = pickle.load(f)
		f.close()
		for i in range(len(X)):
			for j in range(len(X[i])):
				#X[i][j] = - X[i][j]
				pass
	except:
		X = []
		for i in r:
			print i
			X.append([])
			for j in r:
				x = (-10 + 0.125*j)*0.25
				y = (10 - 0.125*i)*0.25
				X[-1].append(testeroid.U(Vector([0.5 + x, 0.5 + y, 0.5])))
			#a.append(testeroid.U(Vector([1, -1, 0])*i + Vector([0.5, 0.5, 0.5])))
			#b.append(testeroid.U(Vector([0, 5, 0.5])*i))
			#c.append(testeroid.U(Vector([5**0.5, 5**0.5, 0.5])*i))
			#d.append(i)
		f = open("gdata.pck", 'w')
		pickle.dump(X, f)
		f.close()
	try:
		f = open("gdata2.pck", 'r')
		Xg = pickle.load(f)
		f.close()
	except:
		Xg = []
		for i in r:
			print i
			Xg.append([])
			for j in r:
				x = -10 + 0.125*j
				y = 10 - 0.125*i
				Xg[-1].append(testeroid.g(Vector([0.5 + x, 0.5 + y, 0.5])))
		f = open("gdata2.pck", 'w')
		pickle.dump(Xg, f)
		f.close()
	print "===="
	x = []
	y = []
	for i in r:
		x.append(-10 + 0.125*i)
		y.append(-(10 - 0.125*i))
	Xn = numpy.matrix(X)[::-1,:]
	ispl = scipy.interpolate.RectBivariateSpline(numpy.array(x), numpy.array(y), Xn)
	xt = numpy.arange(-7, 7, 0.01)
	eps = 0.0001
	print ispl.ev(5,5)
	print ispl.ev(5 + eps,5)
	r2 = range(-9, 10)
	xx, yy = pylab.meshgrid(r2, r2)
	print xx
	print yy
	Gx = numpy.matrix([[0.0]*len(r2)]*len(r2))
	Gy = numpy.matrix([[0.0]*len(r2)]*len(r2))
	s = xx.shape[0] - 1
	for i in range(xx.shape[0]):
		for j in range(xx.shape[0]):
			x2 = xx[i, j]
			y2 = yy[i, j]
			Gx[i,j] = - (ispl.ev(x2 + eps, y2) - ispl.ev(x2, y2))/eps
			Gy[i,j] = - (ispl.ev(x2, y2 + eps) - ispl.ev(x2, y2))/eps

	pylab.grid(True)
	pylab.imshow([[-i for i in l] for l in X], extent = (-10, 10, -10, 10), interpolation='bicubic')
	pylab.colorbar()
	pylab.figure()
	Q = pylab.quiver(numpy.array(xx), numpy.array(yy), numpy.array(Gx), numpy.array(Gy), units='width')
	pylab.axis([-11, 11, -11, 11])
	pylab.title('dfv')
	pylab.show()
	
	gx = Gx[:]
	gy = Gy[:]
	for i in range(xx.shape[0]):
		for j in range(xx.shape[0]):
			gx[i,j] = Xg[i][j].v[0]
			gy[i,j] = Xg[i][j].v[1]
	pylab.grid(True)
	#pylab.show()
	pylab.imshow([[a.v[0] for a in l] for l in Xg], extent = (-10, 10, -10, 10), interpolation='bicubic')
	pylab.colorbar()
	Q = pylab.quiver(numpy.array(xx), numpy.array(yy), numpy.array(gx), numpy.array(gy), units='width')
	pylab.axis([-11, 11, -11, 11])
	pylab.title('gg')
	pylab.show()
	pylab.grid(True)
	pylab.imshow([[a.v[1] for a in l] for l in Xg], extent = (-10, 10, -10, 10), interpolation='bicubic')
	pylab.colorbar()
	raw_input(" > ")
	