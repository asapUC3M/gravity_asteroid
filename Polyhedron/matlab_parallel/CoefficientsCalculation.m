function GeopCoeff = CoefficientsCalculation(C_nm,S_nm,MaxDegree,Sigma,J,NumberFaces)

GeopCoeff = cell(MaxDegree+1,1);

for n = 0 : MaxDegree
    ctn = n+1;
    
    for m = 0 : n
        ctm = m+1;
        
        disp([n m]);
        %%% Cnm coefficients
        aux_c = 0;
        aux_s = 0;
        for f = 1 : NumberFaces
            
            aux_c = aux_c + J(f)*C_nm.Degree(ctn).Order(ctm).Simplex(f).alpha;
            aux_s = aux_s + J(f)*S_nm.Degree(ctn).Order(ctm).Simplex(f).beta;
            
        end
        
        GeopCoeff{ctn}(ctm,1) = real(Sigma*aux_c);
        GeopCoeff{ctn}(ctm,2) = real(Sigma*aux_s);
        

    end

end