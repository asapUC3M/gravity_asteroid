function [alpha,beta] = Alpha_Beta(n,Tri_cnm,Tri_snm,Table_Factorials)

alpha = 0;
beta = 0;

ctn = n+1;

for i = 1 : ctn
    alpha = alpha + 10.^sum(log10(Tri_cnm(i,1:(ctn-i+1))) + Table_Factorials(i,1:(ctn-i+1)));
end

for i = 1 : (n+1)
    beta = beta + 10.^sum(log10(Tri_snm(i,1:(ctn-i+1))) + Table_Factorials(i,1:(ctn-i+1)));
end




end

