function FNF = FaceIntervals(N_Divisions,Body_Faces)

NF = linspace(1,Body_Faces,N_Divisions+1);
NF = floor(NF); NF(end) = Body_Faces;

FNF = nan(N_Divisions,1);

FNF(1,1:2) = [NF(1) NF(2)];
for i = 2 : (N_Divisions)
   FNF(i,1:2) = [(NF(i)+1) NF(i+1)]; 
end

FNF((N_Divisions),2) = Body_Faces;

end

