function [M,Sigma,a,BodyShape] = Data(BodyName)

switch BodyName
        
    case 'Castalia.txt'
        
        M = 5e11;         % Castalia Mass [Kg]
        Sigma = 2.1e3;         % Castalia Density [Kg/m^3];
        a = 900;   % Reference Sphere Radii [m];
        
    otherwise
        
        M = 5.97219e24;         % Earth Mass [Kg]
        Sigma = 5.52e3;         % Earth Density [Kg/m^3];
        a = 6.3781363000e+06;   % Reference Sphere Radii [m];
        
        
end

File = matfile([BodyName(1:(end-4)) '.mat']);
KmToM = 1000;

try

    BodyShape = File.BodyShape;
    BodyShape.Vertex.Loc = BodyShape.Vertex.Loc * KmToM;

catch err
    
    BodyShape = ImportAsteroid(BodyName); 
    
end



end

