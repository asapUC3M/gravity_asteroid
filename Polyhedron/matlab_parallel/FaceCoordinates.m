function [xx,yy,zz] = FaceCoordinates(Body,i)

    xx = Body.Vertex.Loc(Body.Faces.Order(i,:),1)';
    yy = Body.Vertex.Loc(Body.Faces.Order(i,:),2)';
    zz = Body.Vertex.Loc(Body.Faces.Order(i,:),3)';

end

