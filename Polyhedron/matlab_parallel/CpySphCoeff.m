function SphCoeff=CpySphCoeff(InputCoeff,SphCoeff,MaxDegree)

InputCoeff = InputCoeff{2};

for n = 0 : MaxDegree
    ctn = n+1;
    for m = 0 : n
        ctm = m+1;
        SphCoeff{ctn}(ctm,1) = SphCoeff{ctn}(ctm,1) + InputCoeff{ctn}(ctm,1);
        SphCoeff{ctn}(ctm,2) = SphCoeff{ctn}(ctm,2) + InputCoeff{ctn}(ctm,2);
    end
end


end

