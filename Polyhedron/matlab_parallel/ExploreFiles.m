function FilesFound = ExploreFiles()

Files = dir; %List Of Files In Directory

[h,~] = size(Files);

Bodies = cell(h,1);
ct = 1;

for i = 1 : h
    
    w = length(Files(i).name);
    while w > 1
        
        if Files(i).name(w) == '.' && strcmp(Files(i).name(w:end),'.txt') == 1
            
            Bodies{ct} = Files(i).name;
            w = 0;
            ct = ct+1;
            
        else
            w = w-1;
        end
        
        
    end
    
end

FilesFound = cell(ct-1,1);
for i = 1 : (ct-1)
    FilesFound{i} = Bodies{i};
end



