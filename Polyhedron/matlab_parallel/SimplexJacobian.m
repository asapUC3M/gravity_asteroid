function detJ = SimplexJacobian(x_coordinates,y_coordinates,z_coordinates)

% The coordinate arrays must be row vectors

J = [x_coordinates;y_coordinates;z_coordinates];
detJ = det(J);

end

