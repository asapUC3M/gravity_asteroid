function [C_nm, S_nm, NF] = SPH_Coeffs(MaxDegree,BodyShape,M,a,StartF,EndF,Table_Factorials)

%{
    Author: Diego Garc�a Pardo
    UNIVERSITY CARLOS III OF MADRID

    The following code implements the algorithm as described by R.A. Werner
    in his paper about Spherical Harmonics of a Polyhedron Shape.

    Input Variables:

    MaxDegree:  Maximum Degree of the spherical Harmonics
    BodyShape:  Information about triangular mesh along Body Shape
    M:          Mass of the body [Kg]
    a:          Reference Sphere Radious [m]
    Sigma:      Body mean Density [Kg/m^3]


%}

% Normalizing Factors Recursions

N_Sec = @(n)   (2*n-1)/sqrt(2*n*(2*n+1)); %Sectorial Recursion
N_V_1 = @(n,m) (2*n-1)*sqrt((2*n-1)/((2*n+1)*(n+m)*(n-m))); % Vertical Recursion Factor #1
N_V_2 = @(n,m) sqrt((2*n-3)*(n+m-1)*(n-m-1)/((2*n+1)*(n+m)*(n-m))); % Vertical Recursion Factor #2
N_SD = @(n) (2*n-1)/sqrt(2*n+1); %Sub-Diagonal Recursion

f = 1;
NF = StartF : EndF;
for cf = NF
    
    for n = 0 : MaxDegree
        ctn = n+1;
        
        for m = 0 : n
            ctm = m+1;
            
            if (n == m) %Sectorial
                disp([n m 1 f]);
                
                
                [xx,yy,~] = FaceCoordinates(BodyShape,f);
                
                if n == 0
                    
                    [alpha, beta, Trinomial_Cnm, Trinomial_Snm] = Sectorial(n,xx,yy,1/M);
                    
                elseif n == 1
                    
                    [alpha, beta, Trinomial_Cnm, Trinomial_Snm] = Sectorial(n,xx,yy,1/M/sqrt(3)/a);
                    
                else
                    
                    Trinomial_Cnm = C_nm.Degree(ctn-1).Order(ctn-1).Simplex(f).Trinomial;
                    Trinomial_Snm = S_nm.Degree(ctn-1).Order(ctn-1).Simplex(f).Trinomial;
                    [alpha, beta, Trinomial_Cnm, Trinomial_Snm] = Sectorial(n,xx,yy,N_Sec(n)/a,Trinomial_Cnm,Trinomial_Snm,Table_Factorials{ctn});
                    
                end
                
                C_nm.Degree(ctn).Order(ctm).Simplex(f).alpha = alpha;
                S_nm.Degree(ctn).Order(ctm).Simplex(f).beta  = beta;
                
                C_nm.Degree(ctn).Order(ctm).Simplex(f).Trinomial = Trinomial_Cnm;
                S_nm.Degree(ctn).Order(ctm).Simplex(f).Trinomial = Trinomial_Snm;
                
                
                
            elseif (m == (n-1))  %Sub-Diagonal
                disp([n m 2 f]);
                
                [~,~,zz] = FaceCoordinates(BodyShape,f);
                Trinomial_Cnm = C_nm.Degree(ctn-1).Order(ctn-1).Simplex(f).Trinomial;
                Trinomial_Snm = S_nm.Degree(ctn-1).Order(ctn-1).Simplex(f).Trinomial;
                
                [alpha, beta, Trinomial_Cnm, Trinomial_Snm] = SubDiagonal(n,zz,N_SD(n)/a,Trinomial_Cnm,Trinomial_Snm,Table_Factorials{ctn});
                
                
                C_nm.Degree(ctn).Order(ctm).Simplex(f).alpha = alpha;
                S_nm.Degree(ctn).Order(ctm).Simplex(f).beta = beta;
                
                C_nm.Degree(ctn).Order(ctm).Simplex(f).Trinomial = Trinomial_Cnm;
                S_nm.Degree(ctn).Order(ctm).Simplex(f).Trinomial = Trinomial_Snm;
                
            elseif   (m<(n-1)) %Vertical
                disp([n m 3 f]);
                
                [xx,yy,zz] = FaceCoordinates(BodyShape,f);
                
                TriCnm_1 = C_nm.Degree(ctn-1).Order(ctm).Simplex(f).Trinomial;
                TriSnm_1 = S_nm.Degree(ctn-1).Order(ctm).Simplex(f).Trinomial;
                
                TriCnm_2 = C_nm.Degree(ctn-2).Order(ctm).Simplex(f).Trinomial;
                TriSnm_2 = S_nm.Degree(ctn-2).Order(ctm).Simplex(f).Trinomial;
                
                [alpha, beta, Trinomial_Cnm, Trinomial_Snm] = Vertical(n,xx,yy,zz,N_V_1(n,m)/a,N_V_2(n,m)/(a^2),TriCnm_1,TriCnm_2,TriSnm_1,TriSnm_2,Table_Factorials{ctn});
                
                C_nm.Degree(ctn).Order(ctm).Simplex(f).alpha = alpha;
                S_nm.Degree(ctn).Order(ctm).Simplex(f).beta = beta;
                
                C_nm.Degree(ctn).Order(ctm).Simplex(f).Trinomial = Trinomial_Cnm;
                S_nm.Degree(ctn).Order(ctm).Simplex(f).Trinomial = Trinomial_Snm;
            end
        end
    end
    f = f+1;
end

end

