function BODY = Menu

Bodies = ExploreFiles;

fprintf('\n------------------------\n');
fprintf('Spherical Harmonics Computation');
fprintf('\n\nList of available Bodies: \n');

for i =1 : length(Bodies)
    
    fprintf(sprintf([num2str(i) '.) ' Bodies{i}]));
    fprintf('\n');
end

opt = input('Option: ');
BODY = Bodies{opt};

end

