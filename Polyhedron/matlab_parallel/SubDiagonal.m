function [alpha, beta, Trinomial_Cnm, Trinomial_Snm] = SubDiagonal(n,zz,NormalizingFactor,Trinomial_Cnm,Trinomial_Snm,Table_Factorials)

%{

    Author: Diego Garc�a Pardo
    UNIVERSITY CARLOS III OF MADRID

    In the following lines the sub-diagonal recursion is implemented based
    on trinomials.

    See R.A.Wener Spherical Harmonics of a Constant Density Polyhedron

%}

Trinomial_zz = [zz(3) zz(2) 0;zz(1) 0 0;0 0 0]*NormalizingFactor;

Trinomial_Cnm = conv2(Trinomial_zz,Trinomial_Cnm);
Trinomial_Snm = conv2(Trinomial_zz,Trinomial_Snm);

if n == 1
    
    alpha = sum(sum(Trinomial_Cnm))/24;
    beta = sum(sum(Trinomial_Snm))/24;
    
else
    
    [alpha, beta] = Alpha_Beta(n,Trinomial_Cnm,Trinomial_Snm,Table_Factorials);
    
end

end



