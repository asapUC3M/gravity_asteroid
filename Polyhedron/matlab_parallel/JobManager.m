clc
clear all
close all

%% Body Data
BODY = Menu;
[M,Sigma,a,BodyShape] = Data(BODY);
MaxDegree = input('Maximum Degree To Be Computed: ');
%% Load Factorials
Table_Factorials = importdata('TableOfFactorials.mat');

%% Allocate DataStorage
SphCoeff = cell(MaxDegree,1);
for n = 0 : MaxDegree
    ctn = n+1;
    SphCoeff{ctn} = zeros(length(0:n),2);
end

%% Create Cluster Of Analysis

N_Divisions = input('Number Of Divisions: ');
FaceDivisions = FaceIntervals(N_Divisions,BodyShape.Faces.Counter);
c = parcluster;
for i = 1 : N_Divisions
    j(i).Job = batch(@Main,2,{M,Sigma,a,MaxDegree,BodyShape,FaceDivisions(i,1),FaceDivisions(i,2),Table_Factorials});%#ok
    fprintf('Job %i Out Of %i Launched\n',i,N_Divisions);
end

%% Write Results

for n = 0 : MaxDegree
    ctn = n+1;
    SphCoeff{ctn} = zeros(length(0:n),2);
end

for i = 1 : N_Divisions
        wait(j(i).Job);
        
        fprintf('\nJob %i Out Of %i Finished',i,N_Divisions);
        Sol(i).Output = fetchOutputs(j(i).Job);%#ok
        
        SphCoeff=CpySphCoeff(Sol(i).Output,SphCoeff,MaxDegree);
        
        delete(j(i).Job);
       
    
end
fprintf('\n');

save(['SphCoeff_' BODY(1:(end-4)) '_deg' num2str(MaxDegree)],'SphCoeff');
fprintf('\nSaved File SphCoeff.mat\n');
fprintf('---------------------------');
fprintf('\nFINISHED CALCULATION\n');