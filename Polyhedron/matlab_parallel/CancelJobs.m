function CancelJobs(JobList)

L = length(JobList);

for i = 1 : L
    try
        delete(JobList(i).Job);
    catch err
        disp(['Error in Job' num2str(i)]);
        disp(err);
        
    end
end

end