function [t,Job] = Main(M,Sigma,a,MaxDegree,BodyShape,StartF,EndF,Table_Factorials)

%% Geometry Calculations
tic
[C_nm, S_nm, NF] = SPH_Coeffs(MaxDegree,BodyShape,M,a,StartF,EndF,Table_Factorials);
t=toc;

%% Final Coefficients

%%% Jacobian Per Face
J = nan(1,length(NF));
ci = 1;
for i = NF(1) : NF(end)
    [xx,yy,zz] = FaceCoordinates(BodyShape,i);
    J(ci) = SimplexJacobian(xx,yy,zz);
    ci = ci+1;
end

Job = CoefficientsCalculation(C_nm,S_nm,MaxDegree,Sigma,J,length(NF));

end
