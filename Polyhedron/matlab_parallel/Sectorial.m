function [alpha, beta, Trinomial_Cnm, Trinomial_Snm] = Sectorial(n,xx,yy,NormalizingFactor,varargin)

%{

    Author: Diego Garc�a Pardo
    UNIVERSITY CARLOS III OF MADRID

    In the following lines the sectorial recursion is implemented based on
    trinomials.

    See R.A.Wener Spherical Harmonics of a Constant Density Polyhedron

%}
nVarargs = length(varargin);
for i = 1 : nVarargs
    if i == 1
        Trinomial_Cnm  = varargin{i};
    elseif i == 2
        Trinomial_Snm = varargin{i};
    elseif i== 3
        Table_Factorials = varargin{i};
    end
end

%%% Computing Exceptions

if n == 0
    
    Trinomial_Cnm = NormalizingFactor*1;
    Trinomial_Snm = 0;
    
    alpha = NormalizingFactor/6;
    beta = 0;
    
elseif n == 1
    
    Trinomial_Cnm = [xx(3) xx(2) 0;xx(1) 0 0;0 0 0]*NormalizingFactor;
    Trinomial_Snm = [yy(3) yy(2) 0;yy(1) 0 0;0 0 0]*NormalizingFactor;
    
    alpha = sum(sum(Trinomial_Cnm))/24;
    beta = sum(sum(Trinomial_Snm))/24;
    
else
    
    %%% Computing General Terms
    Trinomial_xx = [xx(3) xx(2) 0;xx(1) 0 0;0 0 0]*NormalizingFactor;
    Trinomial_yy = [yy(3) yy(2) 0;yy(1) 0 0;0 0 0]*NormalizingFactor;
       
    aux1 = conv2(Trinomial_xx,Trinomial_Cnm)-conv2(Trinomial_yy,Trinomial_Snm);
    aux2 = conv2(Trinomial_yy,Trinomial_Cnm)+conv2(Trinomial_xx,Trinomial_Snm);
    
    Trinomial_Cnm = aux1;
    Trinomial_Snm = aux2;
    
    [alpha,beta] = Alpha_Beta(n,Trinomial_Cnm,Trinomial_Snm,Table_Factorials);
    
end

end