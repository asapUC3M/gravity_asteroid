#include "Header.h"

double Norm_2(double *V){

	int i = 0;
	double out = 0.0;
	for (i = 0; i < Dimensions; i++){
		out = pow(V[i], 2.0) + out;
	}

	out = sqrt(out);

	return out;

}