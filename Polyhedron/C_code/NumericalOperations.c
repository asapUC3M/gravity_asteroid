#include "Header.h"

void CrossProduct(double V1[Dimensions], double V2[Dimensions], double* Out){


	Out[0] = V1[1] * V2[2] - V2[1] * V1[2];
	Out[1] = V1[2] * V2[0] - V1[0] * V2[2];
	Out[2] = V1[0] * V2[1] - V2[0] * V1[1];


}

double DotProduc(double V1[Dimensions], double V2[Dimensions]){

	double Result = 0;
	int i = 0;

	for (i = 0; i < Dimensions; i++)
		Result = Result + V1[i] * V2[i];

	return Result;

}

double minimum(double *V, unsigned int SizeV){

	unsigned int i = 0;
	double Output;

	Output = V[0];

	for (i = 1; i < SizeV; i++){
		if (Output > V[i])
			Output = V[i];
	}

	return Output;

}

double maximum(double *V, unsigned int SizeV){

	unsigned int i = 0;
	double Output;

	Output = V[0];

	for (i = 1; i < SizeV; i++){
		if (Output < V[i])
			Output = V[i];
	}

	return Output;

}

double Norm_2(double V[Dimensions]){

	double Result = 0;
	//double max = maximum(V, Dimensions);
	unsigned int i = 0;

	//for (i = 0; i < Dimensions; i++)
	//V[i] = V[i] / max;

	for (i = 0; i < Dimensions; i++)
		Result = Result + pow(V[i], 2);

	Result = sqrt(Result);

	return Result;

}

double **Transpose(double **A, unsigned int SS[2]){

	unsigned int i, j;
	double **Result;

	Result = (double**)calloc(SS[1], sizeof(double*));
	for (i = 0; i < SS[1]; i++)
		Result[i] = (double*)calloc(SS[0], sizeof(double));

	for (i = 0; i < SS[0]; i++){
		for (j = 0; j < SS[1]; j++){
			Result[j][i] = A[i][j];
		}
	}

	return Result;

}



double** MatrixMuliplication(double **A, double **B, unsigned int SizeA[2], unsigned int SizeB[2]){

	// SizeA and SizeB are vectors with two [h,w] dimensions

	double **Result;
	unsigned int i = 0;
	unsigned int j = 0;
	unsigned int k = 0;



	if (SizeA[1] == SizeB[0]){

		Result = (double**)(double*)calloc(SizeA[0], sizeof(double*));
		// Allocate Space for Resultant Matrix
		for (i = 0; i < SizeA[0]; i++){
			Result[i] = (double*)calloc(SizeB[1], sizeof(double*));
		}

		// Multiply Matrix

		for (i = 0; i < SizeA[0]; i++){
			for (k = 0; k < SizeA[1]; k++){
				for (j = 0; j < SizeB[1]; j++){

					Result[i][j] = Result[i][j] + A[i][k] + B[k][j];

				}
			}
		}

		return Result;
	}

	else{
		printf("Matrix Dimensions Do Not Match");
		return NULL;
	}
}

void VectorOuterProduct(double ColVector[Dimensions], double RowVector[Dimensions],double Result[Dimensions][Dimensions]){

	int i, j;

	for (i = 0; i < Dimensions; i++){
		for (j = 0; j < Dimensions; j++){
			Result[i][j]= ColVector[i] * RowVector[j];
		}
	}

}

void MatrixVector(double **Matrix, double *Vector, unsigned int MatrixHeight, unsigned int VectorLength,double *Sol){


	unsigned int i, j;

	for (i = 0; i < MatrixHeight; i++){

		for (j = 0; j < VectorLength; j++){

			Sol[i] = Sol[i] + Matrix[i][j] * Vector[j];
			
		}


	}


}

double Row_M_Col(double *Row, double **Matrix, double *Col, unsigned int VectorLength){

	unsigned int i, j, ct;
	double *Aux = (double*)calloc(VectorLength, sizeof(double));

	double Result = 0;
	ct = 0;

	do{

		for (j = 0; j < Dimensions; j++){

			for (i = 0; i < Dimensions; i++){

				Aux[j + ct] = Aux[j + ct] + Row[i + ct] * Matrix[i][j + ct];

			}

		}

		ct = ct + Dimensions;

	} while (ct <= VectorLength);

	for (i = 0; i < VectorLength; i++)
		Result = Result + Aux[i] * Col[i];

	return Result;
}