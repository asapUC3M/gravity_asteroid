#include "Header.h"
#include "Functions_Polyhedron.h"

void PolyhedronParse(Asteroid Shape, E_list *E, double *EE[Dimensions], double *FF[Dimensions]){


	unsigned int i, j, k, ct;
	unsigned int **Checker;

	unsigned int NumberOfEdges = Shape.ctF + Shape.ctV - 2;
	unsigned int EdgeLabel;

	double **NormalToFace, V1[Dimensions], V2[Dimensions], VV[Dimensions], AuxMatrix[3][3], AuxModulus;

	// Matrix Allocation

	Checker = (unsigned int**)calloc((NumberOfEdges), sizeof(unsigned int**));
	for (i = 0; i < NumberOfEdges; i++){
		Checker[i] = (unsigned int*)calloc((NumberOfEdges), sizeof(unsigned int*));
	}

	NormalToFace = (double**)calloc(Shape.ctF, sizeof(double**));
	for (i = 0; i < Shape.ctF; i++){
		NormalToFace[i] = (double*)calloc(Dimensions, sizeof(double*));
	}

	// Shape Parsing
	ct = 0;
	EdgeLabel = 0;

	for (i = 0; i < Shape.ctF; i++){

		for (j = 0; j < Dimensions; j++){
			EdgeLabel = DataFiller(i, j, Checker, E, EdgeLabel, Shape, NumberOfEdges);
		}


		// Compute Normal To Each Face

		///Aqcuire edge vectors
		for (k = 0; k < Dimensions; k++){
			V1[k] = Shape.Vertex[Shape.Faces[i].V[1]].LocXYZ[k] - Shape.Vertex[Shape.Faces[i].V[0]].LocXYZ[k];
			V2[k] = Shape.Vertex[Shape.Faces[i].V[2]].LocXYZ[k] - Shape.Vertex[Shape.Faces[i].V[1]].LocXYZ[k];
		}

		/// Build Unitary Normal
		CrossProduct(V1, V2, VV);
		AuxModulus = Norm_2(VV);
		for (k = 0; k < Dimensions; k++){
			NormalToFace[i][k] = VV[k] / AuxModulus;
		}

		/// Allocate Face Normal Outer Products in Extended Matrix
		VectorOuterProduct(NormalToFace[i], NormalToFace[i], AuxMatrix);

		for (k = 0; k < Dimensions; k++){
			for (j = 0; j < Dimensions; j++){
				FF[k][j + ct] = AuxMatrix[k][j];
			}
		}

		ct = ct + Dimensions;

	}

	// Process E_list E; Process Edge Dyads


	ct = 0; // counter reset

	double V_edge1[Dimensions], V_edge2[Dimensions];
	double VN_edge1[Dimensions], VN_edge2[Dimensions];
	double Modulus1, Modulus2;

	double A[Dimensions][Dimensions], B[Dimensions][Dimensions];

	for (i = 0; i < NumberOfEdges; i++){
	
		// For edge dyad it is required edge vector and Normal To Face

		for (k = 0; k < Dimensions; k++){
			/// Normal to Face Vectors:
			V1[k] = NormalToFace[E[i].FaceNumber1][k];
			V2[k] = NormalToFace[E[i].FaceNumber2][k];

			/// Edge Vectors:
			V_edge1[k] = E[i].Vector1.EndPoint[k] - E[i].Vector1.StartPoint[k];
			V_edge2[k] = E[i].Vector2.EndPoint[k] - E[i].Vector2.StartPoint[k];
		}

		CrossProduct(V_edge1, V1, VN_edge1);
		CrossProduct(V_edge2, V2, VN_edge2);

		// Make Vectors Unitary
		Modulus1 = Norm_2(VN_edge1);
		Modulus2 = Norm_2(VN_edge2);

		for (k = 0; k < Dimensions; k++){
			VN_edge1[k] = VN_edge1[k] / Modulus1;
			VN_edge2[k] = VN_edge2[k] / Modulus1;
		}

		// Compute Outer Products

		VectorOuterProduct(V1, VN_edge1, A);
		VectorOuterProduct(V2, VN_edge2, B);

		// Allocate In Extended Matrix And Dyad

		for (k = 0; k < Dimensions; k++){
			for (j = 0; j < Dimensions; j++){
				

				E[i].Dyad[k][j] = A[k][j] + B[k][j];
				EE[k][j + ct] = E[i].Dyad[k][j];
				

			}
		}

		ct = ct + Dimensions;

	}

	// Freeing Checker and NormalToFace variables

	for (i = 0; i < NumberOfEdges; i++)
		free(Checker[i]);
	

}