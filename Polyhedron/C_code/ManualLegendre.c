#include "Header.h"
#include "Functions_SPH.h"

void ManualLegendre(double P[], double Pn_1[],double Pn_2[], double sinphi, double cosphi){


	P[0] = 1;									// Algorithm Seed
	Pn_1[0] = P[0];

	P[0] = sinphi * Pn_1[0] * sqrt(3.0);		// Legendre Polynomial P(n,m) = P(1,0);
	P[1] = cosphi * Pn_1[0] * sqrt(3.0);		// Legendre Polynomial P(n,m) = P(1,1);

	// Copy Back Results

	Pn_2[0] = Pn_1[0];

	Pn_1[0] = P[0];
	Pn_1[1] = P[1];


}