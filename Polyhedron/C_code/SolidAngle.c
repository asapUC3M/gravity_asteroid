#include "Header.h"
#include "Functions_Polyhedron.h"

void SolidAngle(double PosVector[Dimensions], Asteroid Shape, double *wf, unsigned int NumberOfEdges){

	unsigned int i,j,k;

	double rr[Dimensions][Dimensions], rrN[Dimensions];
	double AuxVector[Dimensions];

	double Num, Den;
	
	for (i = 0; i < Shape.ctF; i++){

		for (j = 0; j < Dimensions; j++){

			for (k = 0; k < Dimensions; k++)
				rr[j][k] = Shape.Vertex[Shape.Faces[i].V[j]].LocXYZ[k] - PosVector[k];

			rrN[j] = Norm_2(rr[j]);

		}

		CrossProduct(rr[1], rr[2], AuxVector);
		Num = DotProduc(rr[0], AuxVector);
		Den = rrN[0] * rrN[1] * rrN[2] + rrN[0] * DotProduc(rr[1], rr[2]) + rrN[1] * DotProduc(rr[2], rr[0]) + rrN[2] * DotProduc(rr[0], rr[1]);

		wf[i] = 2 * atan2(Num, Den);

	}

}