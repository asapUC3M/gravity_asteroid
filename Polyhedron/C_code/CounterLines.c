#include "Header.h"
#include "Functions_Polyhedron.h"

long unsigned int CounterLines(char *file_path){

	FILE *fp = fopen(file_path, "r+");
    long unsigned int line_count = 0;
	char aux = 'D';

    if(fp == NULL){
        return 0;
	}
	else{
		while (fscanf(fp, "%c", &aux) != EOF){
			if (aux == '\n'){
				line_count++;
			}
		}

		fclose(fp);
		return line_count;
	}
}
