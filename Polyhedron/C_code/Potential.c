#include "Header.h"
#include "Functions_Polyhedron.h"

double Potential(double GS, double PosVector[Dimensions], Asteroid Shape, double *EE[Dimensions], double *FF[Dimensions], E_list *E){

	unsigned int NumberOfEdges = Shape.ctF + Shape.ctV - 2;
	double *RRf = (double*)calloc(Shape.ctF * 3, sizeof(double));
	double *RRe = (double*)calloc(NumberOfEdges * 3, sizeof(double));

	double S_E = 0;
	double S_F = 0;
	double Potential_Value;
	double AuxMatrix[Dimensions];

	double *wf = (double*)calloc(Shape.ctF, sizeof(double));
	double *Le = (double*)calloc(NumberOfEdges, sizeof(double));
	
	AuxVectors_Potential(PosVector, Shape, E, RRe, RRf, NumberOfEdges,wf,Le);
	
	unsigned int i, j, k, ct;
	

	ct = 0;

	for (i = 0; i < NumberOfEdges; i++){
		for (k = 0; k < Dimensions; k++){
			
			AuxMatrix[k] = 0;

			for (j = 0; j < Dimensions; j++){
				AuxMatrix[k] = AuxMatrix[k] + RRe[j + ct] * E[i].Dyad[j][k];
			}
			S_E = S_E + AuxMatrix[k] * RRe[k+ct]*Le[i];
		}

		
		ct = ct + Dimensions;
	}
	
	ct = 0;
	for (i = 0; i < Shape.ctF; i++){
		for (k = 0; k < Dimensions; k++){

			AuxMatrix[k] = 0;

			for (j = 0; j < Dimensions; j++){
				AuxMatrix[k] = AuxMatrix[k] + RRf[j + ct] * FF[j][k+ct];
			}
			S_F = S_F + AuxMatrix[k] * RRf[k+ct]*wf[i];
		}

		ct = ct + Dimensions;
	}



	Potential_Value = 0.5*GS*(S_E - S_F);

	free(RRe);
	free(RRf);
	free(wf);
	free(Le);

	return Potential_Value;
	


}