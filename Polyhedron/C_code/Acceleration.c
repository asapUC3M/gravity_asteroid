#include "Header.h"
#include "Functions_Polyhedron.h"

double Acceleration(double GS, double PosVector[Dimensions], Asteroid Shape, double *EE[Dimensions], double *FF[Dimensions], E_list *E, double *AccVector, char NormYesNo){

	unsigned int NumberOfEdges = Shape.ctF + Shape.ctV - 2;
	double *RRf = (double*)calloc(Shape.ctF * 3, sizeof(double));
	double *RRe = (double*)calloc(NumberOfEdges * 3, sizeof(double));

	AuxVectors(PosVector, Shape, E, RRe, RRf, NumberOfEdges);

	double *S_E = calloc(Dimensions, sizeof(double));
	double *S_F = calloc(Dimensions, sizeof(double));

	MatrixVector(EE, RRe, Dimensions, NumberOfEdges * 3, S_E);
	MatrixVector(FF, RRf, Dimensions, Shape.ctF * 3, S_F);

	unsigned int i;

	for (i = 0; i < Dimensions; i++){
		AccVector[i] = GS*(-S_E[i] + S_F[i]);
	}

	if (NormYesNo == 'Y'){
		double AccNorm;
		AccNorm = Norm_2(AccVector);

		return AccNorm;
	}

	else return 0.0;

}