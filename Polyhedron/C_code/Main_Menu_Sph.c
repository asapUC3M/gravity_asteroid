#include "Header.h"
#include "Functions_SPH.h"
#include <ctype.h>


void Init_Coeff_List(struct Coeff_List *list){
	list->size = 0;
	list->root = NULL;
}
int Coeff_InsertInList(struct Coeff_List *list, unsigned int number, char name[], unsigned int degree){

	int check = -1;
	struct Coeff_Description *NewCoeff;
	NewCoeff = (struct Coeff_Description *) malloc(sizeof(struct Coeff_Description));

	if (NewCoeff != NULL){

		// Insertion At The Beginning
		NewCoeff->BodyNumber = number;
		NewCoeff->degree = degree;
		strcpy(NewCoeff->name, name);

		NewCoeff->next = list->root;
		list->root = NewCoeff;
		list->size++;

		check = 1;

	}

	return check;


}


SPH_Coeff CentralBodiesSelection(struct BodyListInfo *Bodies, struct Coeff_List *Coffs, double *GGMM, double *RR, unsigned int *nn,double *ww){

	unsigned int i,j,ct,ct_aux;

	struct Body *ListBody = Bodies->root;
	struct Coeff_Description *ListCoeff = Coffs->root;

	SPH_Coeff sph_mass_coeff;

	system("CLS");
	printf("\n\n");
	printf("--------------List Of Central Bodies Available--------------");

	ct = 0;

	for (i = 0; i < Bodies->size; i++){

		ListCoeff = Coffs->root;

		for (j = 0; j < Coffs->size; j++){

			if (ListBody->Number == ListCoeff->BodyNumber){

				printf("\n%i .- %s Using Coefficients from: %s of degree n: %i", ct, ListBody->BodyName, ListCoeff->name, ListCoeff->degree);
				
				ct++;

			}
			ListCoeff = ListCoeff->next;

		}

		ListBody = ListBody->next;

	}

	printf("\n\n");
	printf("Option: ");
	scanf(" %i", &ct_aux);

	ct = 0;
	ListBody = Bodies->root;
	for (i = 0; i < Bodies->size; i++){

		ListCoeff = Coffs->root;
		for (j = 0; j < Coffs->size; j++){

			if (ListBody->Number == ListCoeff->BodyNumber){

				if (ct == ct_aux){

					printf("\nReading Coefficients");
					sph_mass_coeff = Coefficients_Reader(ListCoeff->name, ListCoeff->degree);
					printf("\nEnded");
					*GGMM = ListBody->GravitationalParameter;
					*RR = ListBody->Ref_Radius;
					*nn = ListCoeff->degree;
					*ww = ListBody->w;

				}
				ct++;
			}
			

			ListCoeff = ListCoeff->next;

		}

		ListBody = ListBody->next;

	}

	return sph_mass_coeff;



}

SPH_Coeff Load_List_Data(double *GGMM, double *RR, unsigned int *nn,double *ww){

	FILE *file;
	char temp[255];

	struct BodyListInfo ListBodies;
	struct Coeff_List ListCoeff;

	InitBodyList(&ListBodies);
	Init_Coeff_List(&ListCoeff);

	unsigned int Number, n;
	char name[50];
	double GM, R, Sigma,w;
	int check;

	file = fopen("List_Bodies.txt", "r+");

	if (file != NULL){

		do{

			fscanf(file, "%s", &temp);

			if (strcmp(temp,"C")==0) {     // If it is a 'C', it is SPH Coeff Description

				fscanf(file, "%s", &temp);
				Number = atoi(temp);

				fscanf(file, "%s", &temp); // Name Retrieval;
				strcpy(name, temp);

				fscanf(file, "%s", &temp);
				n = atoi(temp);

				check = Coeff_InsertInList(&ListCoeff, Number, name, n);

				if (check == -1){
					printf("\nError Inserting In Coefficients List");
				}


			}
			else if (strcmp(temp, "0") != 0 && strcmp(temp, "D") != 0) { // If it is a number, it is body data description

				Number = atoi(temp);

				fscanf(file, " %s", &temp); // Name Retrieval;
				strcpy(name,temp);

				fscanf(file, " %s", &temp); // GM Retrieval;
				GM = atof(temp);

				fscanf(file, " %s", &temp); // R Retrieval;
				R = atof(temp);

				fscanf(file, " %s", &temp); // Sigma Retrieval;
				Sigma = atof(temp);

				fscanf(file, " %s", &temp); // w Retrieval;
				w = atof(temp);

				check = Body_InsertInList(&ListBodies, Number, name, GM, R, Sigma,w);

				if (check == -1){
					printf("\nError Inserting In Body List");
				}

			}
			else
				{
				fscanf(file, " %s", &temp);
				fscanf(file, " %s", &temp);
				fscanf(file, " %s", &temp);
			}


		} while (strcmp(temp,"0") != 0);

		fclose(file);

		// Print List With Parameters

		SPH_Coeff SPH_MassCoefficients = CentralBodiesSelection(&ListBodies, &ListCoeff, GGMM, RR, nn,ww);

		return SPH_MassCoefficients;

	}
	else {
		system("CLS");
		printf("Error Reading List_Bodies.txt");
	}

}

int SPH_Solution_Menu (int opt){

	int CheckFun = -1;

	double GM, R,w;
	unsigned int n;
	SPH_Coeff Coefficients = Load_List_Data(&GM,&R,&n,&w);
	system("CLS");

	if (opt == 1){

		char yn;
		double r, phi, lambda;
		double *Acc;

		yn = Data_Retrieve_SPHCOORDS(&r, &phi, &lambda);

		double *v, cart_position[3], *d2r_fict;
		double x, y, z;
		sph2cart(r, phi, lambda, &x, &y, &z);

		cart_position[0] = x;
		cart_position[1] = y;
		cart_position[2] = z;

		v = (double*)calloc(3, sizeof(double));

		clock_t start = clock();
		Acc = Acc_Field(GM,R,r, phi, lambda, Coefficients.Cnm, Coefficients.Snm, n);

		if (yn == 'Y'){
			d2r_fict = FictAcc(v, cart_position,w);

			unsigned int i;
			for (i = 0; i < 3; i++){
				Acc[i] = Acc[i] - d2r_fict[i];
			}
		}

		double U = SPH_Potential(GM, R, r, phi, lambda, Coefficients.Cnm, Coefficients.Snm, n);

		clock_t end = clock();
		double seconds = (double)(end - start) / CLOCKS_PER_SEC;

		system("CLS");
		printf("\nAcceleration Vector is: \n\n");
		printf("\na_x: %.4E [m/s^2]", Acc[0]);
		printf("\na_y: %.4E [m/s^2]", Acc[1]);
		printf("\na_z: %.4E [m/s^2]", Acc[2]);

		printf("\n\nNorm Of the Vector a = %1.4E [m/s^2]", Norm_2(Acc));

		printf("\n\nThe Potential Energy is: %.4E [J/Kg]", U);

		printf("\n\nExecution Time = %f", seconds);

		printf("\n");
		system("PAUSE");
		CheckFun = 1;

	}
	else if (opt == 2){

		double *InitialState;
		double TimePeriod, TimeStep;
		double *State_Output[6], *T;
		unsigned long int N_steps;
		char yn;

		InitialState = (double*)calloc(6, sizeof(double));

		yn = Data_Retrieve_Integrator(InitialState, &TimePeriod, &TimeStep);

		N_steps = (unsigned long int) (TimePeriod / TimeStep);

		unsigned int i;
		for (i = 0; i < 6; i++) // Reserving Memory
			State_Output[i] = (double*)calloc(N_steps+1, sizeof(double));	

		T = (double*)calloc(N_steps+1, sizeof(double));
		

		system("CLS");
		printf("\nIntegration Initialized");
		RK4_Integrator(TimePeriod, TimeStep, InitialState, State_Output, T, N_steps, GM, R, Coefficients.Cnm, Coefficients.Snm, yn, n,w);
		printf("\nIntegration Finalized");
		printf("\nSaving To ""RK4_Integration.txt"" ");
		Save_To_File("RK4_Integration.txt", State_Output, T, N_steps);

		printf("\n");
		system("PAUSE");


		for (i = 0; i < 6; i++)
			free(State_Output[i]);

		free(InitialState); free(T);

		CheckFun = 1;

	}

	return CheckFun;

}

