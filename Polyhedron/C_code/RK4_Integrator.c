#include "Header.h"
#include "Functions_SPH.h"


void printProgress(double *State[],double T,int i){

	printf("\n");
	printf("\nTime T = %.3E [secs]", T);
	printf("\n x= %.4f [m]  y= %.4f [m] z%.4f [m] \nV_x = %.4f [m/s] V_y %.4f [m/s] V_z = %.4f [m/s]", State[0][i], State[1][i], State[2][i], State[3][i], State[4][i], State[5][i]);
	printf("\n");

}

void RK4_Integrator(double TimePeriod, double TimeStep, double *Initial_State, double *State_Output[6], double *T, unsigned int N_steps, double GM,double R,double *Cnm, double *Snm,char yn,unsigned int Model_Size,double w){

	unsigned int i = 0;
	unsigned int j = 0;
	double XX[6];				// Auxiliary State Vector

	double *k1,*k2,*k3,*k4;

	/* Integrator Functions */

	k1 = (double*) calloc(6,sizeof(double));
	k2 = (double*) calloc(6,sizeof(double));
	k3 = (double*) calloc(6,sizeof(double));
	k4 = (double*) calloc(6,sizeof(double));


	/* Allocate Initial State */
	for (i=0;i<6;i++)
		State_Output[i][0] = Initial_State[i];



	T[0] = 0;



	/* Initialize Integrator */

	for(i=0;i<N_steps;i++){

		//printProgress(State_Output, T[i],i);


		for(j=0;j<=5;j++){
			XX[j] = State_Output[j][i];
		}

		k1 = Field_Integrator(T[i], XX, GM, R, w, Cnm, Snm, Model_Size, yn);

		for(j=0;j<=5;j++){
			XX[j] = State_Output[j][i] + 0.5*k1[j];
		}

		k2 = Field_Integrator(T[i], XX, GM, R, w, Cnm, Snm, Model_Size, yn);

		for(j=0;j<=5;j++){
			XX[j] = State_Output[j][i] + 0.5*k2[j];
		}

		k3 = Field_Integrator(T[i], XX, GM, R, w, Cnm, Snm, Model_Size, yn);

		for(j=0;j<=5;j++){
			XX[j] = State_Output[j][i] + k3[j];
		}

		k4 = Field_Integrator(T[i], XX, GM, R, w, Cnm, Snm, Model_Size, yn);

		for(j=0;j<=5;j++){
			State_Output[j][i+1] = State_Output[j][i] + TimeStep/6.0*(k1[j] + 2.0*k2[j] + 2.0*k3[j] + k4[j]) ;
		}

		T[i + 1] = T[i] + TimeStep;

	}

}