#define G 6.67384e-11									// Universal Gravitational Constant
#define PI 3.14159265358979323846264338327950288419716939937510582097494 // pi
#define Dimensions		3								// Dimensions
#define _CRT_SECURE_NO_WARNINGS 1


#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>
#include<time.h>


// Structures Declaration


typedef struct{

	double *Cnm;
	double *Snm;

}SPH_Coeff;


struct Coeff_Description{

	unsigned int BodyNumber;
	char name[50];
	unsigned int degree;
	struct Coeff_Description *next;

};

struct Coeff_List{
	unsigned int size;
	struct Coeff_Description *root;
};

typedef struct {

	unsigned int V[Dimensions];

}AFaces;

typedef struct {

	double LocXYZ[Dimensions];

}AVertex;

typedef struct {

	AFaces		*Faces;
	AVertex		*Vertex;

	unsigned int ctF, ctV;

}Asteroid;


typedef struct{

	double StartPoint[Dimensions];
	double EndPoint[Dimensions];

}VectorInfo;

typedef struct {

	double Dyad[Dimensions][Dimensions];
	VectorInfo Vector1;
	VectorInfo Vector2;

	unsigned int FaceNumber1;
	unsigned int FaceNumber2;

}E_list;


struct AsteroidList{

	unsigned int BodyNumber;
	long unsigned int PolygonNumber;
	char FileName[255];
	struct AsteroidList *next;

};


struct Body{

	unsigned int Number;
	char BodyName[50];
	double GravitationalParameter;
	double Ref_Radius;
	double Sigma; //density
	double w; // Rotational speed
	struct Body *next;

};

struct BodyListInfo{
	unsigned int size;
	struct Body *root;
};

struct AsteroidListInfo{
	unsigned int size;
	struct AsteroidList *root;
};


// Functions Declaration

void	main();
void	Menu();
void	Selection(int opt);

void InitBodyList(struct BodyListInfo *list);
SPH_Coeff	Coefficients_Reader(char *filename, unsigned int n);
int Body_InsertInList(struct BodyListInfo *list, unsigned int number, char name[], double GM, double R, double Sigma, double w);
double*		FictAcc(double v[], double r[], double w);
char Data_Retrieve_SPHCOORDS(double *r, double *phi, double *lambda);
char Data_Retrieve_Integrator(double InitialState[], double *TimePeriod, double *TimeStep);
int AsteroidMainMenu(int opt);
void Save_To_File(char *FileName, double *Results[6], double* T, int N_steps);
void		cart2sph(double x, double y, double z, double *r, double *phi, double *lambda);
void		sph2cart(double r, double phi, double lambda, double* x, double* y, double* z);

int			SPH_Solution_Menu(int opt);


// Numerical Operations Functions
void CrossProduct(double V1[Dimensions], double V2[Dimensions], double* Out);
double DotProduc(double V1[Dimensions], double V2[Dimensions]);
double minimum(double *V, unsigned int SizeV);
double maximum(double *V, unsigned int SizeV);
double Norm_2(double V[Dimensions]);
double **Transpose(double **A, unsigned int SS[1]);
double **MatrixMuliplication(double **A, double **B, unsigned int SizeA[2], unsigned int SizeB[2]);
void VectorOuterProduct(double ColVector[Dimensions], double RowVector[Dimensions], double Result[Dimensions][Dimensions]);
void printMatrix(unsigned int rows, unsigned int cols, double** Matrix);
unsigned int Map2D1D(unsigned int i, unsigned int j, unsigned int rows);
void PrintAsteroid(Asteroid Shape);
void InitializeLists_And_ExtendedMatrix(E_list *E, double **EE, double **FF, unsigned int ctF, unsigned int ctV);
void MatrixVector(double **Matrix, double *Vector, unsigned int MatrixHeight, unsigned int VectorLength, double *Sol);
