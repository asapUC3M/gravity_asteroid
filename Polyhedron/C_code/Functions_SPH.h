
void		ManualLegendre(double P[], double Pn_1[], double Pn_2[], double sinphi, double cosphi);

void		initializeALF(double* P, double* Pn_1, double* Pn_2, int N);

void		Normalized_Legendre(double P[], double Pn_1[], double Pn_2[], double sinphi, double cosphi, int n);

void		CopyBackRecursion(double P[], double Pn_1[], double Pn_2[], int n);

double*     Field_Integrator(double t, double X[], double GM, double R, double w, double *Cnm, double *Snm, unsigned int Model_Size, char FictAccYN);

double*		Acc_Field(double GM, double R, double r, double phi, double lambda, double *Cnm, double *Snm, unsigned int Model_Size);

double		SPH_Potential(double GM, double R, double r, double phi, double lambda, double *Cnm, double *Snm, unsigned int Model_Size);

void		RK4_Integrator(double TimePeriod, double TimeStep, double *Initial_State, double *State_Output[6], double *T, unsigned int N_steps, double GM, double R, double *Cnm, double *Snm, char yn, unsigned int Model_Size,double w);

void		Save_To_File(char *FileName, double *Results[5], double* T, int N_steps);

void		Sph_Data_Retrieve(unsigned int opt, char *yn, double *r, double *phi, double *lambda);
