#include "Header.h"
#include "Functions_Polyhedron.h"

void InitAstList(struct AsteroidListInfo *list){
	list->size = 0;
	list->root = NULL;
}

int AsteroidInsertion(struct AsteroidListInfo *List, long unsigned int Number, long unsigned int n,char *filename){

	int check = -1;
	struct AsteroidList *New_Asteroid;
	New_Asteroid = (struct AsteroidList *) malloc(sizeof(struct AsteroidList));

	if (New_Asteroid != NULL){

		New_Asteroid->BodyNumber = Number;
		New_Asteroid->PolygonNumber = n;
		strcpy(New_Asteroid->FileName, filename);

		New_Asteroid->next = List->root;
		List->root = New_Asteroid;
		List->size++;

		check = 1;

	}

	return check;

}

Asteroid AsteroidSelection(struct BodyListInfo *ListBodies, struct AsteroidListInfo *ListAsteroid, double *GGMM, double *RR, double *w, double *SS){

	unsigned int i, j, ct, ct_aux;

	struct Body *Bodies = ListBodies->root;
	struct AsteroidList *Asteroids = ListAsteroid->root;

	Asteroid BodyShape;

	system("CLS");
	printf("\n\n");
	printf("--------------List Of Central Bodies Available--------------");

	ct = 0;
	for (i = 0; i < ListBodies->size; i++){

		Asteroids = ListAsteroid->root;
		for (j = 0; j < ListAsteroid->size; j++){

			if (Asteroids->BodyNumber == Bodies->Number){
				
				printf("\n%i .- %s With  %i Polygons", ct, Bodies->BodyName, Asteroids->PolygonNumber);
				
				ct++;
			}
			Asteroids = Asteroids->next;
		}
		Bodies = Bodies->next;
	}


	printf("\n\n");
	printf("Option: ");
	scanf("%i", &ct_aux);

	ct = 0;

	Bodies = ListBodies->root;
	for (i = 0; i < ListBodies->size; i++){

		Asteroids = ListAsteroid->root;
		for (j = 0; j < ListAsteroid->size; j++){

			if (Asteroids->BodyNumber == Bodies->Number){
				
				if (ct == ct_aux){

					printf("\nReading Data File");
					BodyShape = AsteroidTxtRead(Asteroids->FileName);
					printf("\nFinished Reading Data File");
					*GGMM = Bodies->GravitationalParameter;
					*RR = Bodies->Ref_Radius;
					*w = Bodies->w;
					*SS = Bodies->Sigma;

				}

				ct++;
			}
			Asteroids = Asteroids->next;
		}
		Bodies = Bodies->next;
	}


	return BodyShape;

}

Asteroid Asteroid_Load_List_Data(double *GGMM, double *RR, double *ww,double *SS){

	FILE *file;
	char temp[255];

	struct BodyListInfo ListBodies;
	struct AsteroidListInfo ListAsteroid;

	InitBodyList(&ListBodies);
	InitAstList(&ListAsteroid);

	Asteroid BodyShape;

	long unsigned int Number, P;
	char name[50], filename[255];
	double GM, R, Sigma, w;
	int check;

	file = fopen("List_Bodies.txt", "r+");

	if (file != NULL){

		do{

			fscanf(file, "%s", &temp);

			if (strcmp(temp, "D") == 0) {     // If it is a 'D', it is Discrete Body File

				fscanf(file, "%s", &temp);	// Body Number
				Number = atoi(temp);

				fscanf(file, "%s", &temp);  // Number Of Polygons
				P = (int) atof(temp);

				fscanf(file, "%s", &temp);  // FileName Containing Asteroid Information
				strcpy(filename,temp);

				check = AsteroidInsertion(&ListAsteroid, Number, P,filename);

				if (check == -1){
					printf("\nError Inserting In Coefficients List");
				}


			}
			else if (strcmp(temp, "0") != 0 && strcmp(temp, "C") != 0) { // If it is a number, it is body data description

				Number = atoi(temp);

				fscanf(file, " %s", &temp); // Name Retrieval;
				strcpy(name, temp);

				fscanf(file, " %s", &temp); // GM Retrieval;
				GM = atof(temp);

				fscanf(file, " %s", &temp); // R Retrieval;
				R = atof(temp);

				fscanf(file, " %s", &temp); // Sigma Retrieval;
				Sigma = atof(temp);

				fscanf(file, " %s", &temp); // w Retrieval;
				w = atof(temp);

				check = Body_InsertInList(&ListBodies, Number, name, GM, R, Sigma, w);

				if (check == -1){
					printf("\nError Inserting In Body List");
				}

			}
			else if (strcmp(temp, "C") == 0){
				fscanf(file, "%s", &temp);
				fscanf(file, "%s", &temp);
				fscanf(file, "%s", &temp);
			}


		} while (strcmp(temp, "0") != 0);

		fclose(file);

		// Print List With Parameters
		BodyShape = AsteroidSelection(&ListBodies, &ListAsteroid, GGMM, RR, ww, SS);

		return BodyShape;

	}
	else {
		system("CLS");
		printf("Error Reading List_Bodies.txt");
	}

}


void AsteroidFieldComputation(Asteroid Shape, double w, double Sigma,char FictAccYN,double PosVector[3]){

	E_list *E;
	double *EE[Dimensions], *FF[Dimensions];

	unsigned int i, j, k;
	long unsigned int NumberOfEdges = Shape.ctF + Shape.ctV - 2;

	E = malloc(sizeof(E_list)*NumberOfEdges);

	for (i = 0; i < Dimensions; i++){
		EE[i] = (double*)calloc(NumberOfEdges*Dimensions, sizeof(double));
		FF[i] = (double*)calloc(Shape.ctF*Dimensions, sizeof(double));
	}

	for (i = 0; i < NumberOfEdges; i++){

		for (k = 0; k < Dimensions; k++){
			for (j = 0; j < Dimensions; j++){
				E[i].Dyad[k][j] = 0;
			}

			E[i].Vector1.StartPoint[k] = 0;
			E[i].Vector1.EndPoint[k] = 0;

			E[i].Vector2.StartPoint[k] = 0;
			E[i].Vector2.EndPoint[k] = 0;
		}

		E[i].FaceNumber1 = 0;
		E[i].FaceNumber2 = 0;
	}

	PolyhedronParse(Shape, E, EE, FF);

	double GS = G * Sigma;	
	double AccVector[Dimensions], AccNorm, U;

	clock_t start = clock();
	AccNorm = Acceleration(GS, PosVector, Shape, EE, FF, E, AccVector,'Y');
	U = Potential(GS, PosVector, Shape, EE, FF, E);

	if (FictAccYN == 'Y'){
		double *Acc_fict = FictAcc((double*)calloc(3, sizeof(double)), PosVector, w);

		for (i = 0; i < Dimensions; i++){
			AccVector[i] = AccVector[i] - Acc_fict[i];
		}
		free(Acc_fict);
	}

	clock_t end = clock();

	printf("\n\nAcceleration Vector (ax,ay,az) = (%.3E,%.3E,%.3E) [m/s^2]", AccVector[0], AccVector[1], AccVector[2]);
	printf("\nAcceleration Vector Norm, a = %.4E", AccNorm);
	printf("\n\nPotential Energy U = %.4E J", U);

	double seconds = (double)(end - start) / CLOCKS_PER_SEC;

	printf("\n\nExecution Time = %f", seconds);

	for (i = 0; i < Dimensions; i++){
		free(EE[i]);
		free(FF[i]);
	}

	free(E);

	printf("\n");
	system("PAUSE");


}

void AsteroidPropagation(Asteroid Shape, double w, double Sigma, char FictAccYN, double InitialState[], double TimePeriod, double TimeStep){

	double *State_Output[6], *T;
	unsigned long int N_steps;

	unsigned int i, j, k;
	long unsigned int NumberOfEdges = Shape.ctF + Shape.ctV - 2;

	N_steps = (unsigned long int) (TimePeriod / TimeStep);



	for (i = 0; i < 6;i++) // Reserving Memory
		State_Output[i] = (double*)calloc(N_steps+1, sizeof(double));	


	T = (double*)calloc(N_steps+1, sizeof(double));

	E_list *E;
	double *EE[Dimensions], *FF[Dimensions];



	E = malloc(sizeof(E_list)*NumberOfEdges);

	for (i = 0; i < Dimensions; i++){
		EE[i] = (double*)calloc(NumberOfEdges*Dimensions, sizeof(double));
		FF[i] = (double*)calloc(Shape.ctF*Dimensions, sizeof(double));
	}

	for (i = 0; i < NumberOfEdges; i++){

		for (k = 0; k < Dimensions; k++){
			for (j = 0; j < Dimensions; j++){
				E[i].Dyad[k][j] = 0;
			}

			E[i].Vector1.StartPoint[k] = 0;
			E[i].Vector1.EndPoint[k] = 0;

			E[i].Vector2.StartPoint[k] = 0;
			E[i].Vector2.EndPoint[k] = 0;
		}

		E[i].FaceNumber1 = 0;
		E[i].FaceNumber2 = 0;
	}

	PolyhedronParse(Shape, E, EE, FF);

	double GS = G * Sigma;

	RK4_Polyhedron(TimePeriod, TimeStep, InitialState, State_Output, T, N_steps, FictAccYN, w, GS, Shape, EE, FF, E);

	printf("\n\nSaving Results To RK4_Integration_Polyhedron.txt\n\n");

	Save_To_File("RK4_Integration_Polyhedron.txt", State_Output, T, N_steps);
	printf("Results Saved\n");

	for (i = 0; i < Dimensions; i++){
		free(EE[i]);
		free(FF[i]);
	}

	for (i = 0; i < 6; i++)
		free(State_Output[i]);


	free(E); free(T);

}

int AsteroidMainMenu(int opt){


	double GM, R, w, Sigma;
	double r, phi, lambda;
	char yn;
	double PosVector[3];

	Asteroid BodyShape = Asteroid_Load_List_Data(&GM, &R, &w, &Sigma);

	if (opt == 3){

		yn = Data_Retrieve_SPHCOORDS(&r, &phi, &lambda);
		sph2cart(r, phi, lambda, &PosVector[0], &PosVector[1], &PosVector[2]);

		AsteroidFieldComputation(BodyShape, w, Sigma, yn, PosVector);
	
	}
	else if (opt == 4){

		double *InitialState;
		double TimePeriod, TimeStep;
		char yn;

		InitialState = (double*)calloc(6, sizeof(double));
		yn = Data_Retrieve_Integrator(InitialState, &TimePeriod, &TimeStep);

		system("CLS");
		printf("\nIntegration Initialized");
		AsteroidPropagation(BodyShape, w, Sigma, yn, InitialState, TimePeriod, TimeStep);
		printf("\nIntegration Finalized");
		

		free(InitialState);

	}

	return 0;

}