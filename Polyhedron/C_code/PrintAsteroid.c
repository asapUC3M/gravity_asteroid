#include "Header.h"
#include "Functions_Polyhedron.h"

void PrintAsteroid(Asteroid Shape){

	unsigned int i;

	printf("Faces Information");
	
	for (i = 0; i < Shape.ctF; i++){

		printf("Face %i, vertex index are: %i  %i  %i\n", i, Shape.Faces[i].V[0], Shape.Faces[i].V[1], Shape.Faces[i].V[2]);
	}

	printf("\nVertex Information\n");
	getchar();

	for (i = 0; i < Shape.ctV; i++){

		printf("Vertex %i, (x,y,z) = (%f,%f,%f)\n", i, Shape.Vertex[i].LocXYZ[0], Shape.Vertex[i].LocXYZ[1], Shape.Vertex[i].LocXYZ[2]);

	}



}