#include "Header.h"
#include "Functions_Polyhedron.h"

void sort(unsigned int V[2], unsigned int Size){

	unsigned int j = 1;
	unsigned int aux = V[0];

	while (j<Size){

		if (V[j - 1]>V[j]){
			aux = V[j - 1];
			V[j - 1] = V[j];
			V[j] = aux;
			j = 0;
		}

		j++;

	}

}

void printMatrix(unsigned int rows, unsigned int cols,double** Matrix){

	unsigned int i, j;
	printf("\n Printing Matrix .... \n");
	for (i = 0; i < rows; i++){
		for (j = 0; j < cols; j++){
			printf("%i ", Matrix[i][j]);
		}
		printf("\n");
	}
	
}

unsigned int Map2D1D(unsigned int i, unsigned int j,unsigned int rows){

	unsigned int result;
	result = rows*i + j;
	return result;

}

unsigned int DataFiller(unsigned int i, unsigned int j, unsigned int **Checker, E_list *E, unsigned int EdgeLabel, Asteroid AsteroidGeometry, unsigned int NumberOfEdges){

	
	unsigned int k = 0;
	unsigned int Link[2];
	unsigned int Link_aux[2];
	unsigned int EdgeAux;

	if (j < 2){
		Link_aux[0] = AsteroidGeometry.Faces[i].V[j];
		Link_aux[1] = AsteroidGeometry.Faces[i].V[j + 1];
	}

	else if (j == 2){
		Link_aux[0] = AsteroidGeometry.Faces[i].V[2]; // Storing Vertex Employed
		Link_aux[1] = AsteroidGeometry.Faces[i].V[0]; // Storing Vertex Employed
	}

	for (k = 0; k < 2; k++){
		Link[k] = Link_aux[k]; //Copy Back Result 
	}


	sort(Link, 2); // Sortening Vertices Used;

	//printf("%i   %i   %i  (%i,%i) = %i\n", i, j, EdgeLabel, Link[0], Link[1], Checker[Map2D1D(Link[0], Link[1], NumberOfEdges)]);
	if (Checker[Link[0]][Link[1]] == 0){

		E[EdgeLabel].FaceNumber1 = i;

		for (k = 0; k < Dimensions; k++){
			E[EdgeLabel].Vector1.StartPoint[k] = AsteroidGeometry.Vertex[Link_aux[0]].LocXYZ[k];
			E[EdgeLabel].Vector1.EndPoint[k] = AsteroidGeometry.Vertex[Link_aux[1]].LocXYZ[k];
		}

		EdgeLabel = EdgeLabel + 1;
		Checker[Link[0]][Link[1]] = EdgeLabel;

		return EdgeLabel;

	}
	else if (Checker[Link[0]][Link[1]] != (0)){

		EdgeAux = Checker[Link[0]][Link[1]] - 1; // the -1 because we store EdgeLabel +1 in order to be always different from 0
		E[EdgeAux].FaceNumber2 = i;

		for (k = 0; k < Dimensions; k++){
			E[EdgeAux].Vector2.StartPoint[k] = AsteroidGeometry.Vertex[Link_aux[0]].LocXYZ[k];
			E[EdgeAux].Vector2.EndPoint[k] = AsteroidGeometry.Vertex[Link_aux[1]].LocXYZ[k];
		}

		return EdgeLabel;

	}

	else {

		return EdgeLabel;

}
	

	

}
