#include "Header.h"

double* FictAcc(double v[], double r[],double w){


	//Non-Inertial Acceleration In Earth Fixed Frame due to Earth Spin

	//Author: Diego Garc�a Pardo (UNIVERSITY CARLOS III OF MADRID)

	//As the acceleration must be presented in cartesian coordinates, 
	//no especial considerations about gradients must be taken into account

	//It is assumed that the Earth Rotation Speed is Constant
	//It is assumed that the Earth Rotation Speed is w = 7.2921150 e - 5 rads/s 

	//v & r must be column vectors


	double coriolis[3];
	double centrifugal[3];
	double *d2rFict;
	int i = 0;

	d2rFict = (double*) calloc(3,sizeof(double));

	coriolis[0] = 2 * (-v[1]*w);
	coriolis[1] = 2 * v[0]*w;
	coriolis[2] = 2 * 0;

	centrifugal[0] = -r[0]*pow(w,2);
	centrifugal[1] = -r[1]*pow(w,2);
	centrifugal[2] = 0;

	for(i=0;i<3;i++){

		d2rFict[i] = coriolis[i] + centrifugal[i];

	}

	return d2rFict;


}