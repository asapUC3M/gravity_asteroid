#include "Header.h"
#include "Functions_Polyhedron.h"


Asteroid AsteroidTxtRead(char *name){


	Asteroid Shape;
	FILE *file;
	char ID;
	double *Loc[Dimensions]; // Correspond to Vertex Localization (x,y,z) coordinates
	long unsigned int *V[Dimensions]; // Correspond to vertex number
	long unsigned int i = 0;
	long unsigned int j = 0;
	double KmToM = 1000.0;

	long unsigned int NumberOfLines = CounterLines(name);
	long unsigned int aux_num=0;

	char aux[255];

	for (i = 0; i < Dimensions; i++){
		Loc[i] = (double*) calloc(NumberOfLines, sizeof(double));
		V[i] = (long unsigned int*) calloc(NumberOfLines, sizeof(long unsigned int));
	}

	Shape.ctF = 0;
	Shape.ctV = 0;

	file = fopen(name, "r+");

	if (file != NULL) {

		do{

			fscanf(file, "%c", &ID);

			if (ID == 'v'){


				fscanf(file, " %s", &aux);
				Loc[0][Shape.ctV] = atof(aux)*KmToM;

				fscanf(file, " %s", &aux);
				Loc[1][Shape.ctV] = atof(aux)*KmToM;

				fscanf(file, " %s", &aux);
				Loc[2][Shape.ctV] = atof(aux)*KmToM;

				Shape.ctV++;


			}
			else if (ID == 'f'){

				fscanf(file, " %s", &aux);
				V[0][Shape.ctF] = atoi(aux) - 1; // -1 Because Vertex indices start in 0 not in 1

				fscanf(file, " %s", &aux);
				V[1][Shape.ctF]= atoi(aux) - 1;

				fscanf(file, " %s", &aux);
				V[2][Shape.ctF] = atoi(aux) - 1;

				Shape.ctF++;

			}
			else if (ID == '\n'){

				aux_num++;

			}

			

		} while (ID != EOF && (aux_num) <= NumberOfLines);


		Shape.Faces = (AFaces*)calloc(Shape.ctF, sizeof(AFaces));
		Shape.Vertex = (AVertex*)calloc(Shape.ctV, sizeof(AVertex));

		for (i = 0; i < Shape.ctF; i++){
			for (j = 0; j < Dimensions; j++){
				Shape.Faces[i].V[j] = V[j][i];
			}
		}

		for (i = 0; i < Shape.ctV; i++){
			for (j = 0; j < Dimensions; j++){
				Shape.Vertex[i].LocXYZ[j] = Loc[j][i];
			}
		}


		for (i = 0; i < Dimensions; i++){

			free(V[i]);
			free(Loc[i]);
		}

		fclose(file);

	}
	else printf("Failure Opening the File\n");


	

	return Shape;


}