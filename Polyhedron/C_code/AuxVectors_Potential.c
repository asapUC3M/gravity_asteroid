#include "Header.h"
#include "Functions_Polyhedron.h"

void AuxVectors_Potential(double PosVector[Dimensions], Asteroid Shape, E_list *E, double *RRe, double *RRf, unsigned int NumberOfEdges,double *wf, double *Le){

	unsigned int i, j, ct;
	double aa[Dimensions], bb[Dimensions], ee[Dimensions];
	double a, b, e;

	ct = 0;

	// Compute Solid Angle
	SolidAngle(PosVector, Shape, wf, NumberOfEdges);

	for (i = 0; i < Shape.ctF; i++){	// Loop starting from 0 up to Face Count as this is always < NumberOfEdges

		for (j = 0; j < Dimensions; j++){
			RRf[ct + j] = (Shape.Vertex[Shape.Faces[i].V[1]].LocXYZ[j] - PosVector[j]);

			aa[j] = E[i].Vector2.StartPoint[j] - PosVector[j];
			bb[j] = E[i].Vector2.EndPoint[j] - PosVector[j];
			ee[j] = aa[j] - bb[j];

		}

		a = Norm_2(aa);
		b = Norm_2(bb);
		e = Norm_2(ee);

		if ((a + b - e) == 0){
			Le[i] = 0;
		}
		else{
			Le[i] = log((a + b + e) / (a + b - e));
		}


		for (j = 0; j < Dimensions; j++){
			RRe[ct + j] = aa[j];
		}

		ct = ct + Dimensions;

	}

	// For rest of the Edges

	for (i = Shape.ctF; i < NumberOfEdges; i++){	// Loop starting from 0 up to Face Count as this is always < NumberOfEdges

		for (j = 0; j < Dimensions; j++){

			aa[j] = E[i].Vector2.StartPoint[j] - PosVector[j];
			bb[j] = E[i].Vector2.EndPoint[j] - PosVector[j];
			ee[j] = aa[j] - bb[j];

		}

		a = Norm_2(aa);
		b = Norm_2(bb);
		e = Norm_2(ee);

		if ((a + b - e) == 0){
			Le[i] = 0;
		}
		else{
			Le[i] = log((a + b + e) / (a + b - e));
		}



		for (j = 0; j < Dimensions; j++){
			RRe[ct + j] = aa[j];
		}

		ct = ct + Dimensions;

	}

}