#include "Header.h"
#include "Functions_SPH.h"

void cart2sph(double x,double y, double z, double *r, double *phi, double *lambda){


	double rcos;

	rcos = sqrt(pow(x,2)+pow(y,2) );

	*r			= sqrt(pow(rcos,2) + pow(z,2));
	*phi		= atan2(z,rcos);
	*lambda		= atan2(y,x);

}