#include "Header.h"
#include "Functions_Polyhedron.h"

void RK4_Polyhedron(double TimePeriod, double TimeStep, double *Initial_State, double *State_Output[6], double *T, unsigned int N_steps, char yn, double w, double GS, Asteroid Shape, double *EE[Dimensions], double *FF[Dimensions], E_list *E){


	long unsigned int i, j;

	double XX[6];				// Auxiliary State Vector

	double *k1, *k2, *k3, *k4;

	/* Integrator Functions */

	k1 = (double*)calloc(6, sizeof(double));
	k2 = (double*)calloc(6, sizeof(double));
	k3 = (double*)calloc(6, sizeof(double));
	k4 = (double*)calloc(6, sizeof(double));


	/* Allocate Initial State */

	for (i = 0; i < 6;i++)
		State_Output[i][0] = Initial_State[i];


	T[0] = 0;



	/* Initialize Integrator */

	for (i = 0; i<N_steps; i++){

		for (j = 0; j <6; j++){
			XX[j] = State_Output[j][i];
		}

		k1 = Polyhedron_FieldIntegrator(GS, EE, FF, E, Shape, w, XX, yn);
		

		for (j = 0; j <6; j++){
			XX[j] = State_Output[j][i] + 0.5*k1[j];
		}

		k2 = Polyhedron_FieldIntegrator(GS, EE, FF, E, Shape, w, XX, yn);

		for (j = 0; j <6; j++){
			XX[j] = State_Output[j][i] + 0.5*k2[j];
		}

		k3 = Polyhedron_FieldIntegrator(GS, EE, FF, E, Shape, w, XX, yn);

		for (j = 0; j <6; j++){
			XX[j] = State_Output[j][i] + k3[j];
		}

		k4 = Polyhedron_FieldIntegrator(GS, EE, FF, E, Shape, w, XX, yn);

		for (j = 0; j <6; j++){
			State_Output[j][i + 1] = State_Output[j][i] + TimeStep / 6.0*(k1[j] + 2.0*k2[j] + 2.0*k3[j] + k4[j]);
		}

		T[i + 1] = T[i] + TimeStep;

	}

}