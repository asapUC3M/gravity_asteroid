#include "Header.h"
#include "Functions_Polyhedron.h"

double* Polyhedron_FieldIntegrator(double GS, double *EE[Dimensions], double *FF[Dimensions], E_list *E, Asteroid Shape, double w, double StateVector[], char FictAccYN){

	double AccVector[3];
	double PosVector[3];
	unsigned int i;

	for (i = 0; i<3; i++){
		PosVector[i] = StateVector[i];
	}

	Acceleration(GS, PosVector, Shape, EE, FF, E, AccVector, 'N');

	if (FictAccYN == 'Y'){
		double *Acc_fict = FictAcc((double*)calloc(3, sizeof(double)), PosVector, w);

		for (i = 0; i < Dimensions; i++){
			AccVector[i] = AccVector[i] - Acc_fict[i];
		}
		free(Acc_fict);
	}


	double *OutPut;
	OutPut = (double*)calloc(6, sizeof(double));

	for (i = 0; i < 3; i++){

		OutPut[i] = StateVector[i + 3];
		OutPut[i + 3] = AccVector[i];

	}

	// Output Is A Column Vector Made of <v,a> (velocity and Acceleration Vectors)

	return OutPut;

}