#include "Header.h"


void InitBodyList(struct BodyListInfo *list){
	list->size = 0;
	list->root = NULL;
}

int Body_InsertInList(struct BodyListInfo *list, unsigned int number, char name[], double GM, double R, double Sigma, double w){

	int check = -1;
	struct Body *NewBody;
	NewBody = (struct Body *) malloc(sizeof(struct Body));

	if (NewBody != NULL){

		strcpy(NewBody->BodyName, name);
		NewBody->GravitationalParameter = GM;
		NewBody->Ref_Radius = R;
		NewBody->Sigma = Sigma;
		NewBody->Number = number;
		NewBody->w = w;

		// Insertion At The Beginning
		NewBody->next = list->root;
		list->root = NewBody;
		list->size++;

		check = 1;

	}

	return check;


}

char Data_Retrieve_SPHCOORDS(double *r, double *phi, double *lambda){

	char yn;

	system("CLS");
	printf("Inlude Non-Inertial Acceleration (Assuming zero velocity) Y/N: ");
	scanf(" %c", &yn);


	printf("\n\nLocation Parameters (distance,latitude,longitude)");

	printf("\n Distance r [m]: ");
	scanf(" %lf", r);

	printf("\n Latitude [deg]: ");
	scanf(" %lf", phi);
	*phi = *phi * PI / 180.0;

	printf("\n Longitude [deg]: ");
	scanf(" %lf", lambda);
	*lambda = *lambda * PI / 180.0;

	return yn;

}
char Data_Retrieve_Integrator(double InitialState[], double *TimePeriod, double *TimeStep){

		system("CLS");
		printf("Inlude Non-Inertial Acceleration Y/N: ");

		char yn;
		scanf(" %c", &yn);
		printf("\n\nPosition (x,y,z)");

		printf("\n Location in X [m]: ");
		scanf(" %lf", &InitialState[0]);

		printf("\n Location in Y [m]: ");
		scanf(" %lf", &InitialState[1]);

		printf("\n Location in Z [m]: ");
		scanf(" %lf", &InitialState[2]);

		printf("\n\nVelocity (V_x,V_y,V_z)");

		printf("\n Location in V_x [m/s]: ");
		scanf(" %lf", &InitialState[3]);

		printf("\n Location in V_y [m/s]: ");
		scanf(" %lf", &InitialState[4]);

		printf("\n Location in V_z [m/s]: ");
		scanf(" %lf", &InitialState[5]);

		printf("\nTime Period to integrate [secs]: ");
		scanf(" %lf", TimePeriod);
		printf("\nTime Step for RK4 Integrator: ");
		scanf(" %lf", TimeStep);

		return yn;


}

void Menu(){
	
	int opt;
	
	system("CLS");

	printf("\n");
	printf("-----------------------------BACHELOR THESIS-----------------------------");
	printf("\n\n");
	printf("Author: Diego Garcia Pardo (NIA: 100 28 3558)");
	printf("\n\n");
	printf("-----------Analysis And Implementation of Gravity Field Models-----------");
	printf("\n\n");
	printf("Please Select and Option");
	printf("\n");
	printf("\n1.- Spherical Harmonics, Evaluation of Gravity Field");
	printf("\n2.- Spherical Harmonics, Orbit Propagation");
	printf("\n3.- Polyhedral Gravitation, Evaluation of Gravity Field");
	printf("\n4.- Polyhedral Gravitation, Orbit Propagation");
	printf("\n0.- Quit");

	printf("\n\n");
	printf("Option: ");
	scanf("%i", &opt);

	if (opt != 0){

		Selection(opt);

	}

}


void Selection(int opt){

	int check;

	switch (opt){

	case 1:
	{
			  check= SPH_Solution_Menu(opt);

			  system("CLS");
			  printf("------Goodbye------");
			  printf("\n\n");
			  printf("------Thanks------");
			  break;
	}
	case 2:
	{
		
			  check =SPH_Solution_Menu(opt);
			  system("CLS");
			  printf("------Goodbye------");
			  printf("\n\n");
			  printf("------Thanks------");
			  break;
	}

	case 3:
	{
			 check =  AsteroidMainMenu(opt);
			  system("CLS");
			  printf("------Goodbye------");
			  printf("\n\n");
			  printf("------Thanks------");
			  break;
	}

	case 4:
	{
			  check = AsteroidMainMenu(opt);
			  system("CLS");
			  printf("------Goodbye------");
			  printf("\n\n");
			  printf("------Thanks------");
			  break;
	}

	case 0:
	{
			  system("CLS");
			  printf("------Goodbye------");
			  printf("\n\n");
			  printf("------Thanks------");
	}

	default:

		Menu();
		

	}

}
