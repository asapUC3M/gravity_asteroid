#include "Header.h"
#include "Functions_SPH.h"



SPH_Coeff Coefficients_Reader(char *filename,unsigned int n){

	/*  Spherical Harmonics Mass Coefficients Reader

	Input Variables:
		filename : Is the string containing the filename with the spherical harmonics Mass coefficents
		n		 : Is the degree of the model to be openend;
	*/

	unsigned int Coff_Dim = (n + 1 + 1) * (n + 1) / 2; // Sum of arithmetic series to obtain the number of terms

	FILE *file;
	char temp[255];
	SPH_Coeff Coff;
	unsigned int ct,i;

	// Preallocating Data

	Coff.Cnm = (double*)calloc(Coff_Dim, sizeof(double));
	Coff.Snm = (double*)calloc(Coff_Dim, sizeof(double));

	file = fopen(filename, "r+");

	if (file != NULL) {

		i = 0;

		do{

			for (ct = 0; ct < 2; ct++){

				fscanf(file, "%s", &temp);

			}

			fscanf(file, "%s", &temp);
			Coff.Cnm[i] = atof(temp);

			fscanf(file, "%s", &temp);
			Coff.Snm[i] = atof(temp);

			i++;


		} while (i < Coff_Dim);
	}
	else printf("\nError Opening the file");
	
	return Coff;
	
	}