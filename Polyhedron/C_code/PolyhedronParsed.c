#include	"Functions.h"

void PolyhedronParse(Asteroid AsteroidGeometry){
	
	int NumberOfEdges;
	int EdgeLabel;
	int *Link_Check;
	double		 *NormalToface[Dimensions];
	double		 *Extended_NF_OuterProduct[Dimensions];
	double		 *EdgeDyad[Dimensions];
	int ct;
	E_list *E;
	MultipleMatrix Result;
	int i, j, k;

	double AuxModulus,V1[Dimensions], V2[Dimensions],*AuxMatrix,*AuxMatrix2,*VV;

	// Variables Initialization
	NumberOfEdges = AsteroidGeometry.ctF + AsteroidGeometry.ctV - 2; // Euler Formula
	EdgeLabel = 0;
	ct = 0; i = 0; j = 0; k = 0;
	
	// Memory Preallocation

	Link_Check = (int*)		calloc(NumberOfEdges*NumberOfEdges, sizeof(int));
	AuxMatrix = (double*)	calloc(Dimensions*Dimensions, sizeof(double));
	AuxMatrix2 = (double*)	calloc(Dimensions*Dimensions, sizeof(double));
	VV = (double*)			calloc(Dimensions, sizeof(double));
	E = malloc(sizeof(E_list)*NumberOfEdges);

	//////////////////////////////////////////////////////////////////////////
	/////////// Allocate Normal To Face Matrix////////////////////////////////
	//////////////////////////////////////////////////////////////////////////

	for (i = 0; i < Dimensions; i++){
		NormalToface[i] = (double*)calloc(AsteroidGeometry.ctF, sizeof(double*));
	}

	//////////////////////////////////////////////////////////////////////////
	/////////// Allocate Extended Matrices////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////

	for (i = 0; i < Dimensions; i++){
		Extended_NF_OuterProduct[i] = malloc((Dimensions * AsteroidGeometry.ctF)*sizeof(double));
		EdgeDyad[i] = malloc((Dimensions*NumberOfEdges)*sizeof(double));

		for (j = 0; j < (Dimensions*AsteroidGeometry.ctF); j++){
			Extended_NF_OuterProduct[i][j] = 0.0;
		}
		for (j = 0; j < (Dimensions*NumberOfEdges); j++){
			EdgeDyad[i][j] = 0.0;
		}
	}

	//// Allocate to zero the List

	for (i = 0; i < NumberOfEdges; i++){
		for (k = 0; k < Dimensions; k++){
			for (j = 0; j < Dimensions; j++){
				E[i].Dyad[k][j] = 0;
			}

			E[i].Vector1.StartPoint[k] = 0;
			E[i].Vector1.EndPoint[k] = 0;			

			E[i].Vector2.StartPoint[k] = 0;
			E[i].Vector2.EndPoint[k] = 0;
		}

		E[i].FaceNumber1 = 0;
		E[i].FaceNumber2 = 0;
	}

	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////
	//////////////////Start Parsing information///////////////////////////////
	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////

	for (i = 0; i < AsteroidGeometry.ctF; i++){
		printf("\n%i", i);
		// Parse Each Face

		for (j = 0; j < Dimensions; j++){
			// Parse Each Edge of Each Face
			printf("   %i",j);
			EdgeLabel = DataFiller(i, j, Link_Check, E, EdgeLabel, AsteroidGeometry,NumberOfEdges);
			
		}
		// Compute Normal To Each Face

		///Aqcuire edge vectors
		for (k = 0; k < Dimensions; k++){
			V1[k] = AsteroidGeometry.Vertex[AsteroidGeometry.Faces[i].V[1]].LocXYZ[k] - AsteroidGeometry.Vertex[AsteroidGeometry.Faces[i].V[0]].LocXYZ[k];
			V2[k] = AsteroidGeometry.Vertex[AsteroidGeometry.Faces[i].V[2]].LocXYZ[k] - AsteroidGeometry.Vertex[AsteroidGeometry.Faces[i].V[1]].LocXYZ[k];
		}

		/// Build Unitary Normal
		CrossProduct(V1, V2,VV);
		AuxModulus = Norm2(VV);
		for (k = 0; k < Dimensions; k++){
			NormalToface[k][i] = VV[k] / AuxModulus;
		}

		/// Allocate Face Normal Outer Products in Extended Matrix
		VectorOuterProduct(VV, VV, AuxMatrix);
		
		for (k = 0; k < Dimensions; k++){
			for (j = 0; j < Dimensions; j++){
				Extended_NF_OuterProduct[k][j + ct] = AuxMatrix[Map2D1D(k,j,Dimensions)];
			}
		}
		
		ct = ct + Dimensions;

	}

	/////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////
	////////////////////EDGE DYADS COMPUTATION///////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////

	// Process E_list E; Process Edge Dyads
	

	ct = 0; // counter reset
	
	double V_edge1[Dimensions], V_edge2[Dimensions];
	double VN_edge1[Dimensions], VN_edge2[Dimensions];
	double Modulus1, Modulus2;

	double A[Dimensions*Dimensions], B[Dimensions*Dimensions];
	
	for (i = 0; i < NumberOfEdges; i++){
		printf("\nEdge number = %i of %i", i, NumberOfEdges);
		// For edge dyad it is required edge vector and Normal To Face

		for (k = 0; k < Dimensions; k++){
			/// Normal to Face Vectors:
			V1[k] = NormalToface[k][E[i].FaceNumber1];
			V2[k] = NormalToface[k][E[i].FaceNumber2];

			/// Edge Vectors:
			V_edge1[k] = E[i].Vector1.EndPoint[k] - E[i].Vector1.StartPoint[k];
			V_edge2[k] = E[i].Vector2.EndPoint[k] - E[i].Vector2.StartPoint[k];
		}

		CrossProduct(V_edge1, V1, VN_edge1);
		CrossProduct(V_edge2, V2, VN_edge2);

		// Make Vectors Unitary
		Modulus1 = Norm2(VN_edge1);
		Modulus2 = Norm2(VN_edge2);

		for (k = 0; k < Dimensions; k++){
			VN_edge1[k] = VN_edge1[k] / Modulus1;
			VN_edge2[k] = VN_edge2[k] / Modulus1;
		}

		// Compute Outer Products

		VectorOuterProduct(V1, VN_edge1,A);
		VectorOuterProduct(V2, VN_edge2,B);

		// Allocate In Extended Matrix And Dyad

		for (k = 0; k < Dimensions; k++){
			for (j = 0; j < Dimensions; j++){

				E[i].Dyad[k][j] = A[Map2D1D(k, j, Dimensions)] + B[Map2D1D(k, j, Dimensions)];
				EdgeDyad[k][j+ct] = E[i].Dyad[k][j];

			}
		}

		ct = ct + Dimensions;

	}

	// Output Results;
	
	Result.E = malloc(sizeof(E_list)*NumberOfEdges);
	Result.EE = (double**) malloc(sizeof(double**)*Dimensions);
	Result.FF = (double**) malloc(sizeof(double**)*Dimensions);

	//Result = malloc(sizeof(MultipleMatrix));
	
	for (i = 0; i < NumberOfEdges; i++){

		Result.E[i].FaceNumber1 = E[i].FaceNumber1;
		Result.E[i].FaceNumber2 = E[i].FaceNumber2;

		for (k = 0; k < Dimensions; k++){

			Result.E[i].Vector1.StartPoint[k] = E[i].Vector1.StartPoint[k];
			Result.E[i].Vector1.EndPoint[k] = E[i].Vector1.EndPoint[k];
			Result.E[i].Vector2.StartPoint[k] = E[i].Vector2.StartPoint[k];
			Result.E[i].Vector2.EndPoint[k] = E[i].Vector2.EndPoint[k];

			for (j = 0; j < Dimensions; j++){
			Result.E[i].Dyad[k][j] = E[i].Dyad[k][j];
			}
		}
	}



		for (i = 0; i < Dimensions; i++){

		Result.EE[i] = (double*) malloc((NumberOfEdges*Dimensions)*sizeof(double*));
		Result.FF[i] = (double*) malloc((NumberOfEdges*Dimensions)*sizeof(double*));
		
		for (j = 0; j < (Dimensions*NumberOfEdges); j++){
			Result.EE[i][j] = EdgeDyad[i][j];
			Result.FF[i][j] = Extended_NF_OuterProduct[i][j];
		}
	}



}