#include "Header.h"
#include "Functions_SPH.h"

double* Field_Integrator(double t, double X[], double GM, double R, double w, double *Cnm, double *Snm, unsigned int Model_Size, char FictAccYN){


	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//Kinematic & Dynamic Relationships To Produce Vector Of Velocities And
	//Accelerations

	//Author: Diego Garc�a Pardo (UNIVERSITY CARLOS III OF MADRID)

	//Input Definitions:

	//    -- t            : Time Instant (Integrator Time)                                    [s]
	//    -- X            : Vector Of Positions And Velocities At Previous Time Step          [m;m/s]         
	//    -- GM           : Standard Gravitational Constant                                   [m^3 s^-2]
	//    -- w			  : Rotational Speed [rads/s]
	//    --Cnm,Snm   : Geopotential Coefficientes For the Associated
	//                      Planet (FULLY NORMALIZED)                                         
	//    -- Model_Size    : Model Size                                                        []
	//    -- FictAccYN           :  Include or not Fictituous Acceleration (char type)


	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	double v[3], cart_position[3];							// v is the cartesian velocity vector. cart_position is the cartesian location in space										
	int i = 0;
	double r,phi,lambda;
	double* rdotdot;
	double *d2r_fict;
	double state_output[6];

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// Computing Position And Velocity At Given Instant


	for(i=0;i<=2;i++){

		cart_position[i]	= X[i];
		v[i]				= X[i+3];

	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// Transforming Cartesian Coordinates to Spherical Coordinates (Required for Acceleartion Field Algorithm)

	cart2sph(X[0], X[1], X[2], &r, &phi, &lambda);

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// Computing Acceleration Field (Inertial Acceleration)
	
	rdotdot = (double*) calloc(6,sizeof(double));

	rdotdot = Acc_Field(GM, R, r, phi, lambda, Cnm, Snm, Model_Size);  // Acceleration

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// Computing Non Inertial Acceleration

	if (FictAccYN == 'Y'){
		d2r_fict = FictAcc(v, cart_position,w);					// Non Inertial Terms
	}
	else d2r_fict = (double*)calloc(3, sizeof(double));

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// Outputting Results for integration

	for(i=0;i<=2;i++){

		state_output[i]				= v[i];
		state_output[i+3]			= rdotdot[i] - d2r_fict[i];

	}
	
	
	return state_output;											// Column Vector Of velocities And Accelerations


}