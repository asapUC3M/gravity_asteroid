#include "Header.h"
#include "Functions_SPH.h"

void sph2cart(double r,double phi, double lambda, double* x, double* y, double* z){

	double rcos;

	*z =  r * sin(phi); 

	rcos = r * cos(phi);

	*x = rcos * cos(lambda);
	*y = rcos * sin(lambda);


}