
Asteroid		AsteroidTxtRead(char *name);

long unsigned int CounterLines(char *file_path);

void			sort(unsigned int *V, unsigned int Size);

unsigned int	DataFiller(unsigned int i, unsigned int j, unsigned int **Checker, E_list *E, unsigned int EdgeLabel, Asteroid AsteroidGeometry, unsigned int NumberOfEdges);

void			PolyhedronParse(Asteroid Shape, E_list *E, double *EE[Dimensions], double *FF[Dimensions]);

void			SolidAngle(double PosVector[Dimensions], Asteroid Shape, double *wf, unsigned int NumberOfEdges);

void			AuxVectors(double PosVector[Dimensions], Asteroid Shape, E_list *E, double *RRe, double *RRf, unsigned int NumberOfEdges);

double			Acceleration(double GS, double PosVector[Dimensions], Asteroid Shape, double *EE[Dimensions], double *FF[Dimensions], E_list *E, double *AccVector, char NormYesNo);

double*			Polyhedron_FieldIntegrator(double GS, double *EE[Dimensions], double *FF[Dimensions], E_list *E, Asteroid Shape, double w, double StateVector[], char FictAccYN);

double			Potential(double GS, double PosVector[Dimensions], Asteroid Shape, double *EE[Dimensions], double *FF[Dimensions], E_list *E);

void			AuxVectors_Potential(double PosVector[Dimensions], Asteroid Shape, E_list *E, double *RRe, double *RRf, unsigned int NumberOfEdges, double *wf, double *Le);

void			RK4_Polyhedron(double TimePeriod, double TimeStep, double *Initial_State, double *State_Output[6], double *T, unsigned int N_steps, char yn, double w, double GS, Asteroid Shape, double *EE[Dimensions], double *FF[Dimensions], E_list *E);