#include "Header.h"
#include "Functions_SPH.h"

double SPH_Potential(double GM, double R, double r, double phi, double lambda, double *Cnm, double *Snm, unsigned int Model_Size){

	unsigned int j, I, n;

	double out = 0.0;
	double *CC, *SS;
	double *P, *Pn_1, *Pn_2;

	double sinphi, cosphi;

	CC = (double*)calloc(Model_Size, sizeof(double));
	SS = (double*)calloc(Model_Size, sizeof(double));

	sinphi = sin(phi);
	cosphi = cos(phi);

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// Allocating Legendre Vectors



	P = (double*)calloc(Model_Size, sizeof(double));
	Pn_1 = (double*)calloc(Model_Size, sizeof(double));
	Pn_2 = (double*)calloc(Model_Size, sizeof(double));

	ManualLegendre(P, Pn_1, Pn_2, sinphi, cosphi);
	//Normalized_Legendre(P, Pn_1, Pn_2, sinphi, cosphi, 1);

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	I = 0;
	for (n = 0; n < 2; n++){
		for (j = 0; j <= n; j++){

			CC[j] = Cnm[j + I];
			SS[j] = Snm[j + I];


		}
		I = I + n + 1;
	}

	for (n = 2; n < Model_Size;n++){

		Normalized_Legendre(P, Pn_1, Pn_2, sinphi, cosphi, n);
		for (j = 0; j <= n; j++){

			CC[j] = Cnm[j + I];
			SS[j] = Snm[j + I];

			out = out + pow((R / r), (double) n) * P[j] * (CC[j] * cos(j*lambda) + SS[j] * sin(j*lambda));
			

		}

		
		I = I + n + 1;
	}

	out = GM / r * (1+out);
	return out;

}

